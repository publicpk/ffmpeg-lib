#!/bin/bash

#
# show the *first video* stream info of input
#

if [ "$1" == "" ] || [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
	echo "Usage: $0 INPUT_FILE KEY"
	echo ""
	echo "  INPUT_FILE: media file to lookup"
	echo "  KEY: key to lookup. available keys are"
	echo ""
	echo "  Output example: ==>"
	echo '
{
    "streams": [
        {
            "index": 1,
            "codec_name": "aac",
            "codec_long_name": "AAC (Advanced Audio Coding)",
            "profile": "LC",
            "codec_type": "audio",
            "codec_time_base": "1/48000",
            "codec_tag_string": "mp4a",
            "codec_tag": "0x6134706d",
            "sample_fmt": "fltp",
            "sample_rate": "48000",
            "channels": 2,
            "channel_layout": "stereo",
            "bits_per_sample": 0,
            "r_frame_rate": "0/0",
            "avg_frame_rate": "0/0",
            "time_base": "1/48000",
            "start_pts": 0,
            "start_time": "0.000000",
            "duration_ts": 294386688,
            "duration": "6133.056000",
            "bit_rate": "93715",
            "nb_frames": "287487",
            "disposition": {
                "default": 1,
                "dub": 0,
                "original": 0,
                "comment": 0,
                "lyrics": 0,
                "karaoke": 0,
                "forced": 0,
                "hearing_impaired": 0,
                "visual_impaired": 0,
                "clean_effects": 0,
                "attached_pic": 0
            },
            "tags": {
                "creation_time": "2014-02-27 06:27:02",
                "language": "eng",
                "handler_name": "GPAC ISO Audio Handler"
            }
        }
    ]
}
<== End of Sample'
	exit
fi

INPUT=$1
KEY=$2
# for ffprobe options, see https://trac.ffmpeg.org/wiki/FFprobeTips
if [ "$KEY" == "" ]; then
	ffprobe -loglevel error -print_format json -show_streams -select_streams a:0 -i $INPUT
	#echo $RES
else
	RES=$( ffprobe -loglevel error -print_format json -show_streams -select_streams a:0 -i $INPUT )
	RES=$(grep -Po "(?<=\"$KEY\": \")[^\"]*" <<< "$RES")
	echo "$KEY: $RES"
fi


