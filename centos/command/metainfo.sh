#!/bin/bash

#
# show FORMAT of input
#

if [ "$1" == "" ] || [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
	echo "Usage: $1 INPUT_FILE KEY"
	echo "\tINPUT_FILE: media file to lookup"
	echo "\tKEY: key to lookup. available keys are"
	echo "\t   nb_streams"
	echo "\t   nb_programs"
	echo "\t   format_name"
	echo "\t   format_long_name"
	echo "\t   duration"
	echo "\t   size"
	echo "\t   bit_rate"
	echo "\t   probe_score"
	echo "\t   creation_time"
	exit
fi

INPUT=$1
KEY=$2
# for ffprobe options, see https://trac.ffmpeg.org/wiki/FFprobeTips
if [ "$KEY" == "" ]; then
	ffprobe -loglevel error -print_format json -show_format -i $INPUT
	#echo $RES
else
	RES=`ffprobe -loglevel 1 -print_format json -show_format -i $INPUT`
	RES=$(grep -Po "(?<=\"$KEY\": \")[^\"]*" <<< "$RES")
	echo "$KEY: $RES"
fi


#
# Output example
#
# {
#     "format": {
#         "filename": "../performace-test/input/Frozen.2013.1080p.BluRay.x264.mp4",
#         "nb_streams": 2,
#         "nb_programs": 0,
#         "format_name": "mov,mp4,m4a,3gp,3g2,mj2",
#         "format_long_name": "QuickTime / MOV",
#         "start_time": "0.000000",
#         "duration": "6133.055000",
#         "size": "1755005744",
#         "bit_rate": "2289241",
#         "probe_score": 100,
#         "tags": {
#             "major_brand": "isom",
#             "minor_version": "1",
#             "compatible_brands": "isomavc1",
#             "creation_time": "2014-02-27 06:26:54"
#         }
#     }
# }


