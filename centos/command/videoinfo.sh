#!/bin/bash

#
# show the *first video* stream info of input
#

if [ "$1" == "" ] || [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
	echo "Usage: $0 INPUT_FILE KEY"
	echo ""
	echo "  INPUT_FILE: media file to lookup"
	echo "  KEY: key to lookup. available keys are"
	echo ""
	echo "  Output example: ==>"
	echo '
{
    "streams": [
        {
            "index": 0,
            "codec_name": "h264",
            "codec_long_name": "H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10",
            "profile": "High",
            "codec_type": "video",
            "codec_time_base": "125/5994",
            "codec_tag_string": "avc1",
            "codec_tag": "0x31637661",
            "width": 1920,
            "height": 856,
            "coded_width": 1920,
            "coded_height": 864,
            "has_b_frames": 2,
            "sample_aspect_ratio": "0:1",
            "display_aspect_ratio": "0:1",
            "pix_fmt": "yuv420p",
            "level": 41,
            "color_range": "tv",
            "color_space": "bt709",
            "color_primaries": "bt709",
            "chroma_location": "left",
            "refs": 4,
            "is_avc": "1",
            "nal_length_size": "4",
            "r_frame_rate": "24000/1001",
            "avg_frame_rate": "24000/1001",
            "time_base": "1/24000",
            "start_pts": 2002,
            "start_time": "0.083417",
            "duration_ts": 147192045,
            "duration": "6133.001875",
            "bit_rate": "2191418",
            "bits_per_raw_sample": "8",
            "nb_frames": "147045",
            "disposition": {
                "default": 1,
                "dub": 0,
                "original": 0,
                "comment": 0,
                "lyrics": 0,
                "karaoke": 0,
                "forced": 0,
                "hearing_impaired": 0,
                "visual_impaired": 0,
                "clean_effects": 0,
                "attached_pic": 0
            },
            "tags": {
                "creation_time": "2014-02-27 06:26:54",
                "language": "und",
                "handler_name": "video.264#trackID=1:fps=23.976 - Imported with GPAC 0.5.0-rev"
            }
        }
    ]
}
<== End of Sample'
	exit
fi

INPUT=$1
KEY=$2
# for ffprobe options, see https://trac.ffmpeg.org/wiki/FFprobeTips
if [ "$KEY" == "" ]; then
	ffprobe -loglevel error -print_format json -show_streams -select_streams v:0 -i $INPUT
	#echo $RES
else
	RES=$( ffprobe -loglevel error -print_format json -show_streams -select_streams v:0 -i $INPUT )
	RES=$(grep -Po "(?<=\"$KEY\": \")[^\"]*" <<< "$RES")
	echo "$KEY: $RES"
fi


