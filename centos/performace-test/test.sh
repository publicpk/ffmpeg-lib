#!/bin/bash
INPUTS=(
video_1280x720_mp4_h264_1000kbps_25fps_aac_stereo_128kbps_44100hz.mp4 \
video_1280x720_mp4_mpeg4_1000kbps_25fps_aac_stereo_128kbps_44100hz.mp4 \
video_1280x720_webm_vp8_333kbps_25fps_vorbis_stereo_128kbps_44100hz.webm \
video_1280x720_webm_vp9_309kbps_25fps_vorbis_stereo_128kbps_44100hz.webm
)
INPUTS_BIG=(
video_1920x864_h264_BIG.BluRay.mp4
)

INDIR="./input"
OUTDIR="./output"
COUNT=0
LOGFILE=transcode_nohwaccel.$(date +"%y%m%d_%H%M%S").log
FFMPEG="ffmpeg -v error -y"

timestamp() {
  date +"%y%m%d_%H%M%S"
}

currenttime() {
  echo $(( $(date +%s%N) / 1000000 ))
}

elapsedtime() {
  echo $(( $(currenttime) - $1 ))
}

private_memory() {
  echo 0 $(awk '/Private/ {print "+", $2}' /proc/$1/smaps 2>/dev/null) | bc
}

check_memory() {
	local pid=$1
	local memusage=0
	local tmp

	while true; do
		tmp=$(private_memory $pid)
		if [ "$tmp" == "" ] || [ "$tmp" -eq "0" ]; then
			break
		fi
		if [ $tmp -gt $memusage ]; then
			memusage=$tmp
		fi
		sleep 0.5
	done
	echo $memusage
}

dojob() {
	COUNT=$(($COUNT + 1))
	local input="$INDIR/$1"
	local output="$OUTDIR/$COUNT-$1.webm"
	local start=$(currenttime)

	# do job
	#sleep 1 & #
	$FFMPEG -i $input -c:v libvpx -c:a libvorbis $output &

	# check memory
	local pid=$!
	local memusage=$(check_memory "$pid")

	# write log
	local elapsed=$(elapsedtime "$start")
	#count,elapsed(ms),input,output,
	echo "$COUNT, $pid, $memusage, $elapsed, $input" | tee -a $LOGFILE
}

mkdir -p $OUTDIR
echo "# count, pid, memusage(KiB), elapsed time(ms), input, output" | tee $LOGFILE

# small files
for i in {1..1}; do
	for input in ${INPUTS[*]}; do
		dojob $input
	done
done

ffmpeg -version | tee $LOGFILE

# big files
# for i in {1..1}; do
# 	for input in ${INPUTS_BIG[*]}; do
# 		output="$OUTDIR/$i-$input"
# 		input="$INDIR/$input"
# 		dojob $input $output
# 	done
# done
