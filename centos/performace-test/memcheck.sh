#!/bin/bash

private_memory() {
  echo 0 $(awk '/Private/ {print "+", $2}' /proc/$1/smaps 2>/dev/null) | bc
}

check_memory() {
	local pid=$1
	local memusage=0
	local tmp
	local keypress=''

	while [ "x$keypress" != "xq" ]; do
		tmp=$(private_memory $pid)
		if [ "$tmp" == "" ] || [ "$tmp" -eq "0" ]; then
			echo "Process($pid)'s been terminated..."
			break
		fi
		echo "Current : $tmp Kb"
		if [ $tmp -gt $memusage ]; then
			memusage=$tmp
			echo "Current max: $memusage Kb"
		fi
		sleep 1
		keypress="`cat -v`"
	done
	echo "Max memory usage: $memusage Kb"
}

pid=$1
if [ "$pid" == "" ]; then
	echo "Usage: $0 PID"
	exit
fi

if [ -t 0 ]; then stty -echo -icanon -icrnl time 0 min 0; fi

echo "Press 'q' for quit..."
check_memory $pid

if [ -t 0 ]; then stty sane; fi

