#!/bin/bash
INPUTS=(
video_1280x720_mp4_h264_1000kbps_25fps_aac_stereo_128kbps_44100hz.mp4 \
video_1280x720_mp4_mpeg4_1000kbps_25fps_aac_stereo_128kbps_44100hz.mp4 \
video_1280x720_webm_vp8_333kbps_25fps_vorbis_stereo_128kbps_44100hz.webm \
video_1280x720_webm_vp9_309kbps_25fps_vorbis_stereo_128kbps_44100hz.webm
)
INPUTS_BIG=(
video_1920x864_h264_BIG.BluRay.mp4
)

# ===================================================================

timestamp() {
  date +"%y%m%d_%H%M%S"
}

currenttime() {
  echo $(( $(date +%s%N) / 1000000 ))
}

elapsedtime() {
  echo $(( $(currenttime) - $1 ))
}

private_memory() {
  #echo 0 $(awk '/Private/ {print "+", $2}' /proc/$1/smaps 2>/dev/null) | bc
  #echo 0 $(awk '/Rss/ {print "+", $2}' /proc/$1/smaps 2>/dev/null) | bc
  echo $(awk '/VmRSS/ {print $2}' /proc/$1/status 2>/dev/null)
}

check_memory() {
	local pid=$1
	local memusage=0
	local tmp

	while true; do
		tmp=$(private_memory $pid)
		if [ "$tmp" == "" ] || [ "$tmp" -eq "0" ]; then
			break
		fi
		if [ $tmp -gt $memusage ]; then
			memusage=$tmp
		fi
		sleep 0.5
	done
	echo $memusage
}

dojob() {
	COUNT=$(($COUNT + 1))
	local input="$INDIR/$1"
	local output="$OUTDIR/$COUNT-$1.$OUTFORMAT"
	local start=$(currenttime)

	# do job
	#sleep 1 & #
	local iparams=$FFMPEG_IARG
	local oparams=$FFMPEG_OARG
	$FFMPEG $iparams -i $input $oparams $output 2>>$LOGFILE &

	# check memory
	local pid=$!
	local memusage=$(check_memory "$pid")

	# write log
	local elapsed=$(elapsedtime "$start")

	#TODO: save exit code

	echo "$COUNT, $memusage, $elapsed, $input" | tee -a $LOGFILE
}

start() {
	COUNT=0
	mkdir -p $OUTDIR
	echo "# count, mem usage(KiB), elapsed time(ms), input" | tee -a $LOGFILE

	for i in {1..1}; do
		for input in ${INPUTS[*]}; do
			dojob $input
		done
	done
}

# ###############################################################
# H264_vdpau
#
INDIR="./input"
OUTDIR="./output-h264_vdpau"
LOGFILE=./transcode_h264_with_hwaccel.$(date +"%y%m%d_%H%M%S").log
FFMPEG="ffmpeg -v error -y"
OUTFORMAT="mp4"
FFMPEG_IARG="-hwaccel auto -hwaccel_device vdpau"
FFMPEG_OARG="-crf 33 -threads 8 -speed 4 -frame-parallel 1 -c:v libx264 -c:a libfdk_aac -f $OUTFORMAT"
echo "# IN Args: $FFMPEG_IARG" | tee $LOGFILE
echo "# OUT Args: $FFMPEG_OARG" | tee -a $LOGFILE
# See https://trac.ffmpeg.org/wiki/Encode/H.264 for details

start

ffmpeg -version | tee -a $LOGFILE

# ###############################################################
# H264
#
# INDIR="./input"
# OUTDIR="./output-h264"
# LOGFILE=./transcode_without_hwaccel.$(date +"%y%m%d_%H%M%S").log
# FFMPEG="ffmpeg -v error -y"
# OUTFORMAT="mp4"
# FFMPEG_IARG=""
# FFMPEG_OARG="-crf 33 -threads 8 -speed 4 -frame-parallel 1 -c:v libx264 -c:a libfdk_aac -f $OUTFORMAT"
# start

# ###############################################################
# vp8
#
INDIR="./input"
OUTDIR="./output-vp8"
LOGFILE=./transcode_vp8_with_hwaccel.$(date +"%y%m%d_%H%M%S").log
FFMPEG="ffmpeg -v error -y"
OUTFORMAT="webm"
FFMPEG_IARG=""
FFMPEG_OARG="-crf 33 -threads 8 -speed 4 -frame-parallel 1 -c:v libvpx -c:a libvorbis -f $OUTFORMAT"
echo "# IN Args: $FFMPEG_IARG" | tee $LOGFILE
echo "# OUT Args: $FFMPEG_OARG" | tee -a $LOGFILE
# See https://trac.ffmpeg.org/wiki/Encode/VP8 for details

start

ffmpeg -version | tee -a $LOGFILE

# big files
# for i in {1..1}; do
# 	for input in ${INPUTS_BIG[*]}; do
# 		dojob $input
# 	done
# done
