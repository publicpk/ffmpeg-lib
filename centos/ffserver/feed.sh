#!/bin/bash

#INPUT=mov/Frozen.2013.1080p.BluRay.x264.mp4
INPUT=http://192.168.0.9:18090/video

OUTPUT=http://localhost:8090/feed.webm.ffm
OOPTION="-pass 1 -b:v 1400K -crf 33 -threads 8 -speed 4 \
  -tile-columns 6 -frame-parallel 1 -auto-alt-ref 1 -lag-in-frames 25 \
  -an -f webm"


#OUTPUT=http://localhost:8090/feed.mjpeg.ffm

#OUTPUT=http://localhost:8090/feed.flv.ffm

#OUTPUT=http://localhost:8090/feed.mpeg.ffm
#OOPTION="-vcodec libx264 -x264opts level=3.0 -profile:v baseline -preset:v superfast"


#OUTPUT=./data/stream.webm

# ffmpeg -y \
# 	-i $INPUT \
# 	$OOPTION \
# 	$OUTPUT


# ffmpeg -y -i $INPUT -c:v libvpx-vp9 -pass 1 -b:v 1400K -crf 33 -threads 8 -speed 4 \
#   -tile-columns 6 -frame-parallel 1 -auto-alt-ref 1 -lag-in-frames 25 \
#   -an -f webm /dev/null


ffmpeg -y -i $INPUT -c:v libvpx-vp9 -b:v 1400K -crf 33 -threads 8 -speed 2 \
  -tile-columns 6 -frame-parallel 1 -auto-alt-ref 1 -lag-in-frames 25 \
  -c:a libopus -b:a 64k -f webm $OUTPUT

