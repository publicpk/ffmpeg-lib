#!/bin/bash

PNAME=ffserver
if [ "$1" != "" ]; then
	PNAME=$1
fi

echo "Kill $PNAME..."
ps -ef | grep $PNAME | awk '{print $2}' | xargs kill -HUP
# PID=`ps -ef | awk '/${PNAME}/{print $2}'`
# echo "$PID"
# for pid in $(ps -ef | awk '/$PNAME/{print $2}')
# do
# 	echo "Kill $pid"
# 	#kill -HUP $pid
# done
