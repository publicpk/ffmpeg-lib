package main;

import java.net.*;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.io.*;

public class Main {

	static URLConnection getUrlConnection(String url) throws IOException {
		URLConnection conn = new URL(url).openConnection();
		conn.setConnectTimeout(2*1000);
		//conn.setReadTimeout(5*1000);
		
		return conn;
	}
	
	static void printResponseHeader(URLConnection conn) {
		System.out.println("\nURL: " + conn.getURL().toString());
		System.out.println("RESPONSE Header ==>");
		
		Map<String,List<String>> resHdr = conn.getHeaderFields();
		Iterator<String> iter = resHdr.keySet().iterator();
		while ( iter.hasNext() ) {
			String key = iter.next();
			List<String> vals = resHdr.get(key);
			for ( String val : vals ) {
				System.out.println(key + ":" + val + "<<<");
			}
		}
		System.out.println("<== RESPONSE Header\n");
	}
	
	static void printRequestHeader(URLConnection conn) {
		System.out.println("\nURL: " + conn.getURL().toString());
		System.out.println("Request Header ==>");
		
		Map<String,List<String>> reqHdr = conn.getRequestProperties();
		Iterator<String> iter = reqHdr.keySet().iterator();
		while ( iter.hasNext() ) {
			String key = iter.next();
			List<String> vals = reqHdr.get(key);
			for ( String val : vals ) {
				System.out.println(key + ":" + val + "<<<");
			}
		}
		System.out.println("<== Request Header\n");
	}
	
	static void relay(BufferedInputStream in, BufferedOutputStream out) throws IOException {
		// Relay data
		byte[] buffer = new byte[4096];
		int nRead;
		long count = 0;
		while ( (nRead = in.read(buffer)) != -1 ) {
			if ( nRead > 0 ) {
				out.write(buffer, 0, nRead);
			}
			//			try {
				Thread.yield();
			// } catch (InterruptedException e) {
			// 	e.printStackTrace();
			// }
			//System.out.print("(" + count + ", " + nRead + ")");
			// if ( count++ % 5 == 0 )
			// 	System.out.println();
		}		
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println("Start...");
		if ( args.length < 2 ) {
			System.out.println("Usage: main.Main INPUT OUTPUT");
			return;
		}
		String input = args[0]; // from camera
		String output = args[1]; // to server
		System.out.println("INPUT: " + input + "\nOUTPUT: " + output);
		
		URLConnection inputConn = getUrlConnection(input);
		URLConnection outputConn = getUrlConnection(output);
		
		printRequestHeader(inputConn);
		printRequestHeader(outputConn);
		
		outputConn.setDoOutput(true);
		inputConn.connect();
		outputConn.connect();
		
		printResponseHeader(inputConn);
		
		BufferedInputStream in = new BufferedInputStream(inputConn.getInputStream());
		BufferedOutputStream out = new BufferedOutputStream(outputConn.getOutputStream());
		//BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(output));
		
		relay(in, out);

		//printResponseHeader(outputConn);
		
		in.close();
		out.close();
		
		System.out.println("Finished...");
	}
}
