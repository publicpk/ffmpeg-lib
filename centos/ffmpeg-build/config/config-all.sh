#!/bin/bash

_WORKING_DIR=`pwd`

_BUILD_LIBS_INFO=( # name version url extension
# "yasm" "0" "git://github.com/yasm/yasm.git" "git"
"yasm" "1.3.0" "http://www.tortall.net/projects/yasm/releases/yasm-1.3.0.tar.gz" "gz"
"x264" "0" "git://git.videolan.org/x264" "git"
"x265" "0" "https://bitbucket.org/multicoreware/x265" "hg"
"fdk-aac" "0" "git://git.code.sf.net/p/opencore-amr/fdk-aac" "git"
"mp3lame" "3.99.5" "http://downloads.sourceforge.net/project/lame/lame/3.99/lame-3.99.5.tar.gz" "gz"
"opus" "0" "git://git.opus-codec.org/opus.git" "git"
"ogg" "1.3.2" "http://downloads.xiph.org/releases/ogg/libogg-1.3.2.tar.gz" "gz"
"vorbis" "1.3.4" "http://downloads.xiph.org/releases/vorbis/libvorbis-1.3.4.tar.gz" "gz"
"vpx" "0" "https://chromium.googlesource.com/webm/libvpx.git" "git"
"ffmpeg" "n2.7.3" "git://source.ffmpeg.org/ffmpeg" "git"
"" "" "" ""
)

SRC_DIR=$_WORKING_DIR/../../ffmpeg.git
SRC_VER="n2.7.3"

# ///////////////////////////////////////////
# 3rd-party libraries
#
LIBSRC_DIR=$_WORKING_DIR/../../3rd-parties
BUILD_LIB_DEPS=(yasm x264 x265 fdk-aac mp3lame opus ogg vorbis vpx)

INSTALL_DIR=$HOME/ffmpeg-build/all-n2.7.3
BIN_DIR=$INSTALL_DIR/bin

FFMPEG_CONFIG="\
	--enable-gpl \
	--enable-nonfree \
	--enable-libfdk-aac \
	--enable-libx264 \
	--enable-libx265 \
	--enable-libmp3lame \
	--enable-libvorbis \
	--enable-libvpx \
	--enable-libopus \
	--enable-vdpau \
	--enable-x11grab \
	--enable-hwaccel=h263_vaapi \
	--enable-hwaccel=h264_vdpau \
	--enable-hwaccel=mpeg4_vaapi \
	--enable-hwaccel=h263_vdpau \
	--enable-hwaccel=hevc_dxva2 \
	--enable-hwaccel=mpeg4_vdpau \
	--enable-hwaccel=h264_dxva2 \
	--enable-hwaccel=mpeg1_vdpau \
	--enable-hwaccel=vc1_dxva2 \
	--enable-hwaccel=mpeg1_xvmc \
	--enable-hwaccel=vc1_vaapi \
	--enable-hwaccel=h264_qsv \
	--enable-hwaccel=mpeg2_dxva2 \
	--enable-hwaccel=vc1_vdpau \
	--enable-hwaccel=h264_vaapi \
	--enable-hwaccel=mpeg2_vaapi \
	--enable-hwaccel=wmv3_dxva2 \
	--enable-hwaccel=h264_vda \
	--enable-hwaccel=mpeg2_vdpau \
	--enable-hwaccel=wmv3_vaapi \
	--enable-hwaccel=h264_vda_old \
	--enable-hwaccel=mpeg2_xvmc \
	--enable-hwaccel=wmv3_vdpau \
	--enable-shared \
	--enable-pic \
	--enable-postproc \
	--enable-version3 \
	--enable-avresample \
	--disable-static \
	--disable-doc \
"
#
#   --enable-libwebp \
#	--enable-hwaccel=h264_mmal \
#


#
# util function
#
function lib_info() {
	local name="$1"
	local num=${#_BUILD_LIBS_INFO[*]}
	local i
	local ret=""
	for (( i=0; i<=$(( $num -1 )); i+=4 )); do
		if [ "$name" == "${_BUILD_LIBS_INFO[$i]}" ]; then
			ret="${_BUILD_LIBS_INFO[$i]} 			\
				${_BUILD_LIBS_INFO[$(( $i+1 ))]} 	\
				${_BUILD_LIBS_INFO[$(( $i+2 ))]} 	\
				${_BUILD_LIBS_INFO[$(( $i+3 ))]}"
			break
		fi
	done
	echo $ret
}

#
# generator
#
function gen_vars() {
	echo "# generating make file format..."
	echo "WORKING_DIR=$_WORKING_DIR"
	echo "INSTALL_DIR=$INSTALL_DIR"
	echo "BIN_DIR=$BIN_DIR"
	echo "LIBSRC_DIR=$LIBSRC_DIR"
	echo "SRC_DIR=$SRC_DIR"
	echo "BUILD_LIB_DEPS=\"$BUILD_LIB_DEPS\""
}

