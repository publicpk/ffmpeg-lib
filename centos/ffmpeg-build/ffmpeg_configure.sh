#!/bin/bash

source ./config.sh
source ./get_3rd_libs.sh

# Download and checkout FFmpeg source
#source ./get_ffmpeg.sh

# Download and build libraries
./build_3rd_libs.sh

# check out ffmepg source
# info=$(lib_info "ffmpeg")
# info=($info) # to array
# libversion="${info[1]}"
checkout_git $SRC_DIR $SRC_VER

# ///////////////////////////////////////////
# configure FFMpeg
#
pushd $SRC_DIR

PATH="$BIN_DIR:$PATH" PKG_CONFIG_PATH="$INSTALL_DIR/lib/pkgconfig" ./configure \
	--prefix="$INSTALL_DIR" \
	--extra-cflags="-I$INSTALL_DIR/include" \
	--extra-ldflags="-L$INSTALL_DIR/lib" \
	--bindir="$BIN_DIR" \
	$FFMPEG_CONFIG \
  | tee $INSTALL_DIR/configure-centos.$SRC_VER.log

popd


