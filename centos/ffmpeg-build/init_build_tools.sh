#!/bin/sh

# ##############################
# Initialize build tools
# ##############################

sudo yum update
sudo yum install autoconf automake cmake freetype-devel gcc gcc-c++ git 
sudo yum libtool make mercurial nasm pkgconfig zlib-devel

# remove packages
# TODO
#yum -y remove ffmpeg x264 libav-tools libvpx-dev libx264-dev

# install necessary tools & packages
# TODO
#yum -y --force-yes install autoconf automake build-essential libtool libva-dev libvdpau-dev libxcb1-dev libxcb-shm0-dev libxcb-xfixes0-dev pkg-config texi2html zlib1g-dev git
