#!/bin/bash

source ./config.sh
source ./get_3rd_libs.sh

#
# Build 3rd-party libraries
#
install_lib() {
	if [ "${flag}" == "" ] || [ "${flag}" == "0" ]; then
		if [ -d $libdir ]; then
			echo "$libdir: cleaning build files"
			pushd $libdir
			$cleancmd
			popd
		else
			echo "$libname: removing package"
			#sudo apt-get purge yasm
		fi
		return
	fi

	# check package
	installed=$(pkg-config --libs-only-l $libname)
	if [ "$installed" != "" ]; then
		echo "$libname: already installed: $installed."
		return
	fi
	if [ "${flag}" == "1" ]; then
		echo "$libname: compiling from source"
		pushd $libdir
		$buildcmd1
		$buildcmd2
		$buildcmd3
		popd
	else # 2
		echo "$libname: installing package"
		# install
		# sudo apt-get install $libname
	fi
}

install_ogg() {
	libname="ogg"
	flag="$1"
	libdir="$2"

	cleancmd="make distclean"
	# getcmd1="wget http://downloads.xiph.org/releases/ogg/libogg-1.3.2.tar.xz"
	# getcmd2="tar xJvf libogg-1.3.2.tar.xz"
	buildcmd1="./configure --prefix=$INSTALL_DIR --enable-shared --disable-static"
	buildcmd2="make install > /dev/null"
	buildcmd3=""

	install_lib
}

install_vorbis() {
	# has ogg dependency
	install_ogg $1

	libname="vorbis"
	flag="$1"
	libdir="$2"

	cleancmd="make distclean"
	#getcmd1="git clone --depth 1 git://pkgs.fedoraproject.org/libvorbis.git libvorbis"
	#getcmd2=""
	# getcmd1="wget http://downloads.xiph.org/releases/vorbis/libvorbis-1.3.5.tar.xz"
	# getcmd2="tar xJvf libvorbis-1.3.5.tar.xz"
	buildcmd1="./configure --prefix=$INSTALL_DIR --enable-shared --disable-static"
	buildcmd2="make install > /dev/null"
	buildcmd3=""
	install_lib
}

install_webp() {
	libname="libwebp"
	flag="$1"
	libdir="$2"

	cleancmd="make distclean"
	# getcmd1="git clone --depth 1 --branch v0.4.3 https://chromium.googlesource.com/webm/libwebp libwebp-0.4.3"
	# getcmd2=""
	buildcmd1="autoreconf -fiv"
	buildcmd2="./configure --prefix=$INSTALL_DIR --enable-shared --disable-static"
	buildcmd3="make install > /dev/null"
	install_lib
}

install_mp3lame() {
	libname="mp3lame"
	flag="$1"
	libdir="$2"

	cleancmd="make distclean"
	# getcmd1="wget http://downloads.sourceforge.net/project/lame/lame/3.99/lame-3.99.5.tar.gz"
	# getcmd2="tar xzvf lame-3.99.5.tar.gz"
	buildcmd1="./configure --prefix=$INSTALL_DIR --enable-nasm --enable-shared --disable-static"
	buildcmd2="make install > /dev/null"
	buildcmd3=""

	install_lib

	if [ "${flag}" == "1" ]; then
	    echo '
# pkg-config file from libmp3lame v3.99.5
prefix=/home/centos/dev01/ffmpeg-build
exec_prefix=${prefix}
libdir=${prefix}/lib
includedir=${prefix}/include

Name: mp3lame
Description: mp3lame
Version: 3.99.5
Requires:
Conflicts:
Libs: -L${libdir} -lmp3lame -lm
Libs.private: -lm -lpthread
Cflags: -I${includedir}
' > $INSTALL_DIR/lib/pkgconfig/mp3lame.pc
	fi
}

install_vpx() {
	libname="vpx"
	flag="$1"
	libdir="$2"

	cleancmd="make distclean"
	# getcmd1="git clone --depth 1 --branch v1.4.0 https://chromium.googlesource.com/webm/libvpx libvpx-1.4.0"
	# getcmd2=""
	buildcmd1="./configure --prefix=$INSTALL_DIR --enable-shared --disable-static --disable-examples --disable-docs --disable-unit_tests"
	buildcmd2="make install"
	buildcmd3=""

	install_lib
}

install_yasm() {
	libname="yasm"
	flag="$1"
	libdir="$2"

	cleancmd="make distclean"
	buildcmd1="./configure --prefix=$INSTALL_DIR --bindir=$BIN_DIR"
	buildcmd2="make install > /dev/null"
	buildcmd3=""
	if [ "${flag}" != "" ] && [ "${flag}" != "0" ] && [ -s "$BIN_DIR/yasm" ]; then
	    echo "$libname: already installed"
	    return
	fi

	install_lib

	# TODO
	# need to check
}

install_x264() {
	# See the following URL for stable release version
	# http://git.videolan.org/?p=x264.git;a=summary
	libname="x264"
	flag="$1"
	libdir="$2"

	cleancmd="make distclean"
	# getcmd1="git clone --depth 1 git://git.videolan.org/x264 x264.git"
	# getcmd2=""
	buildcmd1="./configure --prefix=$INSTALL_DIR --bindir=$BIN_DIR --enable-shared --enable-pic"
	buildcmd2="make install > /dev/null"
	buildcmd3=""

	install_lib

	# TODO
	# fetch the stable version
}

install_fdk-aac() {
	libname="fdk-aac"
	flag="$1"
	libdir="$2"

	cleancmd="make distclean"
	# getcmd1="git clone --depth 1 --branch v0.1.4 https://github.com/mstorsjo/fdk-aac.git fdk-aac-0.1.4"
	# getcmd2=""
	buildcmd1="autoreconf -fiv"
	buildcmd2="./configure --prefix=$INSTALL_DIR --enable-shared --disable-static"
	buildcmd3="make install"

	install_lib
}

init() {
	# check 3rd party lib directory
	if [ ! -d $LIBSRC_DIR ]; then
		mkdir -p $LIBSRC_DIR
	fi

	if [ ! -d $INSTALL_DIR ]; then
		mkdir -p $INSTALL_DIR
	fi

	echo "Installing 3rd party libraries to ${INSTALL_DIR}"
	
	export PKG_CONFIG_PATH=${INSTALL_DIR}/lib/pkgconfig
	export PATH=$BIN_DIR:$PATH
}

init

pushd $LIBSRC_DIR

for name in $INSTALL_LIBS; do
	echo "install ${name}..."
	install_${name} 2
done

for name in "${BUILD_LIB_DEPS[@]}"; do
	echo "compile and install ${name}..."

	info=$(lib_info $name)
	# echo $info

	# IFS=' ' read -r -a info <<< "$info"
	info=($info) # to array

	libname="${info[0]}"
	libversion="${info[1]}"
	liburl="${info[2]}"
	libext="${info[3]}"

	# download
	libdir=$(get_3rd_lib $liburl $libversion $libext $libname)

	echo $libdir
	if [ "x${libdir}" == "x" ]; then
		echo "downloading failed...: ${name}"
		exit 1
	fi

	# build
	install_${name} 1 $libdir
done

# for name in $BUILD_LIBS; do
# 	echo "compile and install ${name}..."
# 	install_${name} 1
# done

for name in $CLEAN_LIBS; do
	echo "clean ${name}..."
	install_${name} 0
done

popd

