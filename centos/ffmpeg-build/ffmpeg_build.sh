#!/bin/bash

source ./config.sh

# ///////////////////////////////////////////
# build FFMpeg
#
pushd $SRC_DIR

PATH="$BIN_DIR:$PATH" make
make install
hash -r

popd
