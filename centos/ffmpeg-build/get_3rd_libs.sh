#! /bin/bash

#
# Download 3rd-party libraries
#


# PWD=`pwd`

function get_3rd_lib() {
	local TAG="$0"
	local url="$1"
	local version="$2"
	local extension="$3"
	local name="$4"

	local savename=${url##*/}
	local git_make_branch="-b"
	local result=0

	if [ "x${extension}" == "x" ]; then
		# extension is not specified
		# try to find the extension from the url
		# extension=${url##*/}
		extension=${savename##*.}
	fi
	# [ "x${extension}" == "x" ] && echo "${TAG}:!!! no extention specified" && exit 1

	if [ "x${name}" == "x" ]; then
		# name is not specified
		# try to find the name from the url
		#name=${url##*/}
		name=${savename%.*}
		# remove .tar
		[ ${name##*.} == "tar" ] && name=${name%.*}
	fi
	# [ "x${name}" == "x" ] && echo "${TAG}:!!! no name specified" && exit 1

	# when the extension is git or hg, 
	# - append "-git" if has specific version
	# - append "-master" if not
	if [[ "${extension}" == "git" || "${extension}" == "hg" ]]; then
		[ "x${version}" != "x0" ] && name="${name}-git"
		[ "x${version}" == "x0" ] && name="${name}-master"
	else
		[ "x${version}" != "x0" ] && name="${name}-${version}"
	fi

	# echo "${TAG}:>>> url is '${url}'"
	# echo "${TAG}:>>> version is '${version}'"
	# echo "${TAG}:>>> extension is '${extension}'"
	# echo "${TAG}:>>> name is '${name}'"

	#
	# Download sources
	#
	function _download() {
		result=0
		case $extension in
			git ) 
				if [ "x${version}" != "x0" ]; then
					# TODO: find better clone method for specific version
					git clone $url $name
					pushd $name
					git checkout $git_make_branch $version
					popd
				else
					git clone --depth 1 $url $name
				fi
				;;
			gz ) [ -e $savename ] || curl -L $url -o $savename
				mkdir -p $name
				tar -xzf $savename -C $name --strip 1
				;;
			xz ) [ -e $savename ] || curl -L $url -o $savename
				mkdir -p $name
				tar -xJf $savename -C $name --strip 1
				;;
			hg ) # TODO
				;&
			tar ) # TODO
				;&
			* ) 
				# echo "${TAG}:!!! invalid extension: ${extension}"
				result=1
				;;
		esac
		echo "$result"
	}

	if [ -e $name ]; then
		# echo "${TAG}:>>> '${name}' is already existed..."

		if [[ "$extension" == "git" && "x${version}" != "x0" ]]; then
			pushd $name
			git checkout $git_make_branch $version
			popd
		fi
		result=0
	else
		result=$(_download)
	fi

	# if success, return directory name
	if [ "x${result}" == "x0" ]; then
		echo "$name"
	else
		echo ""
	fi
}

function checkout_git() {
	local dir="$1"
	local version="$2"

	if [ ! -e "$dir/.git" ]; then
		echo "!!! $dir is not a git directory"
		return 1
	fi

	pushd $dir
		git checkout $version
	popd
	return 0
}

# url="$1"

# if [[ "x${url}" == "x" || "x${url}" == "x-h" || "x${url}" == "x--help" ]]; then
# 	echo "Usage:"
# 	echo "$0 download_url [version] [extension] [name]"
# 	echo "Example:"
# 	echo "$0 'git://github.com/yasm/yasm.git' '' 'git' 'yasm'"
# else
# 	get_3rd_lib "$@"
# fi

