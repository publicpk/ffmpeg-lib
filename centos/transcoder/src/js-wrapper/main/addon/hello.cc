/*
 * hello.cc
 *
 *  Created on: Nov 27, 2015
 *      Author: peterk
 */


#include <node.h>

using namespace v8;

Handle<Value> Method(const Arguments& args) {
  HandleScope scope;
  return scope.Close(String::New("world"));
}

void Initialize(Handle<Object> exports) {
  exports->Set(String::NewSymbol("hello"),
      FunctionTemplate::New(Method)->GetFunction());
}

NODE_MODULE(hello, Initialize)
