/*
 * transcoder-js.cc
 *
 *  Created on: Nov 27, 2015
 *      Author: peterk
 */

#include <node.h>
#include <v8.h>

//#include "nan.h"

#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>

extern "C" {
#include "transcode.h"
}

using namespace v8;

Handle<Value> Init(const Arguments& args) {
	HandleScope scope;

//	if (args.Length() < 1) {
//		ThrowException(Exception::TypeError(String::New("Wrong number of arguments")));
//		return scope.Close(Undefined());
//	}
//
//	if ( !args[0]->IsString() ) {
//		ThrowException(Exception::TypeError(String::New("Wrong arguments")));
//		return scope.Close(Undefined());
//	}

	char* cLogInfo = NULL;

	if ( args.Length() > 0 && args[0]->IsString() ) {
		String::Utf8Value arg(args[0]->ToString());
		cLogInfo = *arg;
	}

	avtranscode_init(cLogInfo);

	return scope.Close(Undefined());
}

Handle<Value> Deinit(const Arguments& args) {
	HandleScope scope;
	avtranscode_deinit();
	return scope.Close(Undefined());
}

Handle<Value> Create(const Arguments& args) {
	HandleScope scope;

	Local<Number> res = Number::New((long)avtranscode_create_job());
	return scope.Close(res);
}

Handle<Value> Destroy(const Arguments& args) {
	HandleScope scope;

	if (args.Length() < 1) {
		ThrowException(Exception::TypeError(String::New("Wrong number of arguments")));
		return scope.Close(Undefined());
	}

	if ( !args[0]->IsNumber() ) {
		ThrowException(Exception::TypeError(String::New("Wrong arguments")));
		return scope.Close(Undefined());
	}

	Local<Number> instance = args[0]->ToNumber();

	avtranscode_destroy_job((TranscodeJob*)((long)instance->Value()));

	return scope.Close(Undefined());
}

Handle<Value> Core(const Arguments& args) {
	HandleScope scope;
	//jint argc, jobjectArray argv, jlong instance;

	if (args.Length() < 3) {
		ThrowException(Exception::TypeError(String::New("Wrong number of arguments")));
		return scope.Close(Undefined());
	}

	if ( !args[0]->IsNumber() || !args[1]->IsArray() || !args[2]->IsNumber() ) {
		ThrowException(Exception::TypeError(String::New("Wrong arguments")));
		return scope.Close(Undefined());
	}

	Local<Number> res;
	Local<Integer> argc = args[0]->ToInteger();
	Local<Array> argv = Local<Array>::Cast(args[1]);
	Local<Number> instance = args[2]->ToNumber();

	const int cargc = argc->Value();
	char** cargv = new char*[cargc];
	int i;
    for ( i=0; i<cargc; i++) {
    	String::Utf8Value arg(argv->Get(Integer::New(i))->ToString());
    	cargv[i] = strdup(*arg);
    }

    int cres = core_main(cargc, (char**)cargv, (TranscodeJob*)((long)instance->Value()));
    res = Number::New(cres);

	printf(">>> Core(): RES is %d\n", cres);

	// release
    for ( i=0; i < cargc; i++) free(cargv[i]);
	delete [] cargv;

	return scope.Close(res);
}

void Initialize(Handle<Object> exports) {
	exports->Set(String::NewSymbol("init"),
			FunctionTemplate::New(Init)->GetFunction());

	exports->Set(String::NewSymbol("deinit"),
			FunctionTemplate::New(Deinit)->GetFunction());

	exports->Set(String::NewSymbol("create"),
			FunctionTemplate::New(Create)->GetFunction());

	exports->Set(String::NewSymbol("destroy"),
			FunctionTemplate::New(Destroy)->GetFunction());

	exports->Set(String::NewSymbol("core"),
			FunctionTemplate::New(Core)->GetFunction());
}

NODE_MODULE(transcoderjs_addon, Initialize)
