{
  "targets": [
    {
      "target_name": "transcoderjs_addon",

      "include_dirs": [
        "<!(node -e \"require('nan')\")",
      	"build/libtranscoder"
      ],

      "sources": [ 
      	"addon/transcoder-js.cc" 
	  ],

      "libraries": [
      	 "/home/cto7/work.bucket/ffmpeg/centos/transcoder/src/js-wrapper/main/build/libtranscoder/Release/libtranscoder.so"
      ]
    },
    {
      "target_name": "action_after_build",
      "type": "none",
      "dependencies": [ "transcoderjs_addon" ],
      "copies": [
          {
            "files": [ "<(PRODUCT_DIR)/<(module_name).node" ],
            "destination": "<(module_path)"
          }
      ]
    }
  ]
}
