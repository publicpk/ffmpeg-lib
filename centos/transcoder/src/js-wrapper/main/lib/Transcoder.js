/**
 * Transcoder.js
 */

//var addon = require('./build/Release/transcoderjs_addon');

var binary = require('node-pre-gyp');
var path = require('path');
//var binding_path = binary.find(path.resolve(path.join(__dirname,'../package.json')), {"module_root":"."});
var binding_path = binary.find(path.resolve(path.join(__dirname,'../package.json')));
var addon = require(binding_path);

module.exports.init = addon.init;

module.exports.deinit = addon.deinit;

module.exports.open = function open() {
	var handle = addon.create();
	return handle;
}

module.exports.isOpened = function isOpened(handle) {
	return (handle != 0);
}

module.exports.close = function(handle) {
	addon.destroy(handle);
}

module.exports.process = function(handle, argc, argv) {
	
	console.log("Transcoder.process(): %d", argc);
	console.log("Transcoder.process(): %s", argv);
	var ret = addon.core(argc, argv, handle);
	if ( ret < 0 ) {
		console.log("!!!!!!!!!!!!!: process(): ret: %d", ret);
	}
	return ret >= 0;
}

/**
 * # Simple Usage #
 * 
 * var transcoder = require("transcoderjs");
 * 
 * transcoder.init();
 * 
 * var handle = transcoder.open();
 * if ( ! transcoder.isOpened(handle) ) 
 * 		return;
 * 
 * var argv = ["scale", "-i", "input.mp4", "-vf", "scale=100:200", "output.mp4"];
 * var result = transcoder.process(handle, argv.length, argv);
 * 
 * transcoder.close(handle);
 * transcoder.deinit();
 * 
 * result == true ? console.log("success") : console.log("failure");
 * 
 **/
