var TAG = 'media-server';

var http = require('http');
var debug = require('debug')(TAG);

var config = require('./config').config;
var appName;
var server;

function main(name) {
  appName = name;

  // load the app
  var app = require('./' + appName);
  var appConfig = config[appName];

  // create server
  server = http.createServer(app);

  // listen
  server.listen(appConfig.port, function onListening() {
    var addr = server.address();
    var bind;
    if ( addr ) {
      bind = typeof addr === 'string' ?
          'pipe ' + addr :
          'port ' + addr.port;
    }
    else {
      bind = appConfig.port + "(null)";
    }
    debug(appName + ': ' + 'Listening on ' + bind);
  });
  server.on('error', onError);

  return 0;
}

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(appName + ': ' + bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(appName + ': ' + bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

// get app name which to start
appName = process.argv[2];
if ( appName ) {
  main(appName);
}

module.exports = main;
