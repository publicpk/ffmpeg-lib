/**
 * Created by peterk on 12/2/15.
 */


var transcoder = require('transcoderjs');
var Q = require('q');

//
// Job Implementation
//
/* extending */
function extend(child, parent) {
  for (var key in parent) {
    if (Object.prototype.hasOwnProperty.call(parent, key)) {
      child[key] = parent[key];
    }
  }
  function ctor() { this.constructor = child; }
  ctor.prototype = parent.prototype;
  child.prototype = new ctor;
  child.__super__ = parent.prototype;
  return child;
}

//
// Job Info
//
function JobInfo(type, id, input, callback, output) {
    this.type = type;
    this.id = id;
    this.input = input;
    this.callback = callback;
    this.output = output;
    this.state = "init";
}

JobInfo.prototype.start = null; // abstract

//
// Scale Job Info
//

// extends JobInfo
function ScaleJobInfo(/* See JobInfo constructor */) {
	ScaleJobInfo.__super__.constructor.apply(this, arguments);
}
extend(ScaleJobInfo, JobInfo);

ScaleJobInfo.prototype.setParams = function(scaleParam) {
    // TODO: check the validation of 'scaleParam'
    this.param = scaleParam;
    return true;
};

ScaleJobInfo.prototype.start = function() {
    var deferred = Q.defer();
    var self = this;
    this.state = "running";

    process.nextTick(function() {
        var ret = -1;
        var handle = transcoder.open();
        if ( transcoder.isOpened(handle) ) {
            var targv = [self.type,
                // input
                "-i", self.input,
                // scale
                "-vf", "scale=" + self.param,
                // output
                self.output];

            ret = transcoder.process(handle, targv.length, targv);
        }
        //debug("ScaleJobInfo.start(): ret is " + ret);
        if ( ret < 0 ) {
            // failure
            deferred.reject(ret);
        }
        else {
            // success
            deferred.resolve(ret);
        }
    });
    // async process
    return deferred.promise;
};

//
// Crop Job Info
//

// extends JobInfo
function CropJobInfo(/* See JobInfo constructor */) {
	CropJobInfo.__super__.constructor.apply(this, arguments);
}
extend(CropJobInfo, JobInfo);

CropJobInfo.prototype.setParams = function(cropParam) {
    // TODO: check the validation of 'cropParam'
    this.param = cropParam;
    return true;
};

CropJobInfo.prototype.start = function() {
    // TODO
};

module.exports.ScaleJobInfo = ScaleJobInfo;
module.exports.CropJobInfo = CropJobInfo;


/*
//
// Shape
//
var Shape = function (name) {
    this.name = name;
}
Shape.prototype.draw = null; // abstract

// extends Shape
var Circle = function () {
	Circle.__super__.constructor.apply(this, arguments);
};
extend(Circle, Shape);

Circle.prototype.draw = function() {
	console.log(this.name + ": draw circle...");
}

// extends Shape
var Triangle = function () {
	Triangle.__super__.constructor.apply(this, arguments);

}
extend(Triangle, Shape);

Triangle.prototype.draw = function() {
	console.log(this.name + ": draw triangle...");
}

module.exports.Shape = Shape;
module.exports.Circle = Circle;
module.exports.Triangle = Triangle;
*/
