
# Transcoding Server Interface

## Data Type
    - {{media_file_path}}: file path or url
    - {{media_file_data}}: MIME data
    - {{callback_url}}: URL
    - {{status_code}}: number
    - {{status_message}}: string
    - {{result_count}}: number
    - {{job_id}}: 
    - {{scale_info}}: width:height format
    - {{area_info}}: width:height:x:y format


## JSON Data Type    
    - {{response_json}}: JSON object
```json
        {
          "status": {{status_code}},
          "message": {{status_message}},
          "count": {{result_count}},
          "result": {{request_specific_json_object}} OR {{request_specific_json_array}} 
        }
```

## Job List
### Request
    - method: GET
    - URL pattern: /job[/{{job_id}}
### Response
    - status code: 
    - body: {{response_json}}
    - {{request_specific_json_object}}: {{job_list_result}}
```json
        {
          "job_id": {
            "type": {{job_type}},
            "id": {{job_id}},
            "input": {{media_file_path}},
            "callback": {{callback_url}},
            "output": {{media_file_path}},
            "state": {{job_state_value}},
            "scaleParam": {{scale_info}}
          }
        }
```

## Scaling Job
### Request
    - method: POST
    - URL pattern: /scale?scale={{scale_info}} [&callback={{callback_url}}] [&input={{media_file_path}}]
    - body: [{{media_file_data}}]
### Response
    - {{response_json}}

## Cropping Job 
### Request
    - method: POST
    - URL pattern: /crop?area={{area_info}} [&callback={{callback_url}}] [&input={{media_file_path}}]
    - body: {{media_file_path}} OR {{media_file_data}}
### Response
    - {{response_json}}

