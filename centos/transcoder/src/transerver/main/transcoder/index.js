//
// Transcoding Server
//

/**
 * ###########################################################
 *  Transcoding Server
 * ###########################################################
 *
 */

var TAG = "transcoding-server";

//
// modules
//
var express = require('express');
var bodyParser = require('body-parser');

var fs = require('fs');
var path = require('path');
var pass = require('stream').PassThrough;
var shortid = require('shortid');
var debug = require('debug')(TAG);

var Q = require('q');
var transcoder = require('transcoderjs');
var jobInfo = require('./job');

// config
var config = require('../config').config;

//
// const values
//
var feedRoot = config.transcoder.outputDir;

//
// global variables
//
var app = express();
var router = express.Router();

var feeds = {};

app.set('json replacer', function(key, val) {
	// ignore properties which starts with '_'. such as '_process', '_handle', etc.
	return (key.charAt(0) === '_') ? undefined : val;
});
app.use(bodyParser.json(null)); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

// list of job available
router.get('/job/:id*?', function(req, res) {
    var id = req.params.id;
    var result;
    if ( id ) {
        result = {};
        result[id] = feeds[id];
    } else {
        result = feeds;
    }
    var status = id && result[id] == null ? 404 : 200;
    var resObj = new ResponseObject(status, "feed list", result);
    res.jsonp(resObj);
});

// POST(feeder)
router.post('/scale', function(req, res) {
    var resObj = createScaleJob(req, res);
    res.jsonp(resObj);
});

// GET(consumer)
router.get('/crop', function(req, res) {
    // TODO
    res.jsonp(new ResponseObject(404, "not implemented", null));
});

// Misc
router.get('/kill', function(req, res) {
	res.status(200).end("good bye~");
	global.process.nextTick(function() {
		//global.process.send({cmd:'shutdown', sig:'SIGINT'});
		global.process.exit();
	});
});

//
// register router
//
app.use(config.transcoder.prefix, router);

function _firstElementKey(objs) {
    var ret;
    for ( ret in objs ) if (objs.hasOwnProperty(ret)) break;
    return ret;
}

function processJob() {
    var key = _firstElementKey(feeds);
    if ( key == null ) { // no elements
        //debug(">>> processJob(): no job ");
        return;
    }

    var job = feeds[key];
    if ( job.state === "running" ) {
        debug(">>> processJob(): job is running ");
        return;
    }

    debug(">>> processJob(): start job, " + key);
    var promise = job.start()
        .then(function(ret) {  // success
            debug(">>> processJob(): success return, " + ret);
            // call callback
        }, function(ret) {     // failure
            debug("!!! processJob(): failure return, " + ret);
            // call callback
        })
        .then(function () {
            // do next job
            delete feeds[key];
            debug(">>> processJob(): job deleted, " + key);
        });
}

function ResponseObject(status, message, result) {
    this.status = status;
    this.message = message;
    this.count = result ? Object.keys(result).length : 0;
    this.result = result;
}

function createScaleJob(req, res) {
    var status = 200;
    var message = "ok";
    var result = {};

    var scale = req.param("scale"),
        input = req.param("input"),
        callback = req.param("callback");

    if ( scale == null || input == null ) {
        status = 400;
        message = "invalid parameters";
    } else {
        var id = "scale_" + shortid.generate();
        var type = "scale";
        var output = feedRoot + "/" + id + "$$" + type + "$$" + path.basename(input);

        result[id] = new jobInfo.ScaleJobInfo(type, id, input, callback, output);
        result[id].setParams(scale);
        feeds[id] = result[id];
    }

    return new ResponseObject(status, message, result);
}

// directories
fs.exists(feedRoot, function(exists) {
    if ( !exists ) fs.mkdir(feedRoot);
});

//
// Start process job timer
//
// TODO: Use cluster worker for processing job
var processIntervalId = setInterval(processJob, 1000);
transcoder.init();

//
// exit handler
//
function onExit(exitCode) {
    console.log(TAG, ': closing feeds...', exitCode);
    for ( var feed in feeds ) {
        if ( feeds.hasOwnProperty(feed) )
            feed.close();
    }
    clearInterval(processIntervalId);
    feeds = {};
    transcoder.deinit();
}

global.process
    .on('message', function(msg/*, param*/) {
        if ( !msg || !msg.cmd )
            return;
        if ( msg.cmd === 'kill' || msg.cmd === 'shutdown' ) {
            onExit(msg.sig);
            global.process.exit();
        }
    })
    .on('close', function(exitCode) {
        onExit('SIGINT');
        console.log(TAG + ": closed... %d", exitCode);
    })
    .on('exit', function(exitCode) {
        onExit('SIGINT');
        console.log(TAG + ": terminated... %d", exitCode);
    });

//
// exports
//
module.exports = app;

