var feedRoot = __dirname + '/data';

exports.config = {
	"transcoder": {
		port: 8090,
		prefix: '/transcode',
		host: 'localhost',
		outputDir: './out'
	}
};
