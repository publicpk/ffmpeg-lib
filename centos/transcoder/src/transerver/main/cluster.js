var cluster = require('cluster');
var numCPUs = 1;
//var numCPUs = require('os').cpus().length;
//var config = require('./config').config;

if ( cluster.isWorker ) {
    console.log("Worker %s(%d) starting...", cluster.worker.id, cluster.worker.process.pid);
	var server = require('./app');
    var name = "transcoder";
    var ret = server(name);
    if ( ret != 0 )
        console.error("Error while running server: %d, %s", ret, name);
}
else if (cluster.isMaster) {
    for (var i = 0; i < numCPUs; i++) {
        cluster.fork()
            .on('exit', function(code, signal) {
                console.log('worker %s(%d) died (%s)', this.id, this.process.pid, signal || code);
            })
            .on('error', function (err) {
                console.log('worker %s(%d) has error, (%s)', this.id, this.process.pid, err);
            });
    }

    cluster.on('exit', function(worker, code, signal) {
        console.log('worker ' + worker.process.pid + ' died: ' + code + ', ' + signal);
    });

    // Keep track of http requests
    //var numReqs = 0;
    //setInterval(function() {
    //    console.log("numReqs =", numReqs, ", mem = ", global.process.memoryUsage().heapUsed / 1024 / 1024);
    //}, 1000);
    //
    //// Count requests
    //function messageHandler(msg) {
    //    if (msg.cmd && msg.cmd == 'notifyRequest') {
    //        numReqs += 1;
    //    }
    //}
    //Object.keys(cluster.workers).forEach(function(id) {
    //    cluster.workers[id].on('message', messageHandler);
    //});
}
else {
    console.log("Who am I ? ");
}
