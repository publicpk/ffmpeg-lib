#include <stdio.h>

#include <libavformat/avformat.h>
#include <libavutil/dict.h>
#include "libavutil/threadmessage.h"
#include "libavutil/eval.h"

#include "cmdutils.h"
#include "transcode.h"

int get_metadata(const char *inputFile, AVMetadata *metadata)
{
    AVFormatContext *fmt_ctx = NULL;
    //AVDictionaryEntry *tag = NULL;

    AVCodecContext *codec_ctx = NULL;

    int ret, i;

    av_register_all();

    metadata->hasVideoStream = metadata->hasAudioStream = -1;
    // open input file
    if ((ret = avformat_open_input(&fmt_ctx, inputFile, NULL, NULL)) != 0 ) {
    	av_log(NULL, AV_LOG_FATAL, "!!! get_metadata(): Could not open input file. %d\n", ret);
        return ret;
    }

    // retrieve stream information
    if( (ret = avformat_find_stream_info(fmt_ctx, NULL)) < 0 ) {
    	av_log(NULL, AV_LOG_FATAL, "!!! get_metadata(): Could not find stream info. %d\n", ret);
    	goto __exit; // Couldn't find stream information
    }

    // Find the video and audio stream
    for( i=0; i<fmt_ctx->nb_streams; i++ ) {
    	if( fmt_ctx->streams[i]->codec->codec_type==AVMEDIA_TYPE_VIDEO ) {
        	// Get a pointer to the codec context for the video stream
    		codec_ctx = fmt_ctx->streams[i]->codec;

        	metadata->hasVideoStream = 1;
        	metadata->width = codec_ctx->width;
        	metadata->height = codec_ctx->height;
        	metadata->videoCodecId = codec_ctx->codec_id;
    	}
    	else if ( fmt_ctx->streams[i]->codec->codec_type==AVMEDIA_TYPE_AUDIO ) {
        	// Get a pointer to the codec context for the audio stream
    		codec_ctx = fmt_ctx->streams[i]->codec;

        	metadata->hasAudioStream = 1;
        	metadata->audioCodecId = codec_ctx->codec_id;
    	}
    	if ( metadata->hasVideoStream != -1 && metadata->hasAudioStream != -1 )
    		break;
    }

//    if( metadata->hasVideoStream != -1 ) {
//    	printf("width x height: %d x %d\n", metadata->width, metadata->height);
//    	printf("video codec id: %d\n", metadata->videoCodecId);
//    }
//
//    if( metadata->hasAudioStream != -1 ) {
//    	printf("audio codec id: %d\n", metadata->audioCodecId);
//    }
//
//    while ((tag = av_dict_get(fmt_ctx->metadata, "", tag, AV_DICT_IGNORE_SUFFIX)))
//        printf("%s=%s\n", tag->key, tag->value);

__exit:
    //
    // release
    avformat_close_input(&fmt_ctx);

    return ret;
}
