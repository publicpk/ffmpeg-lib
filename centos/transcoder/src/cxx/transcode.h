
#ifndef TRANSCODE_H
#define TRANSCODE_H

#include <pthread.h>

#define REPORT_ENV_NAME "AVREPORT"
#define EXIT_PROG_RET(n) return n
#define EXIT_PROG_GOTO(n) goto n
#define EXIT_PROG_BRK break

#define EXIT_PROG_FAIL -99

// TODO fix this return
#define EXIT_PROG_NORET return

typedef struct AVMetadata {
	int hasVideoStream;
	int hasAudioStream;
	int width;
	int height;
	int videoCodecId;
	int audioCodecId;
} AVMetadata;

struct InputStream;
struct InputFile;
struct OutputStream;
struct OutputFile;
struct FilterGraph;
struct SwsContext;
struct AVDictionary;

typedef struct TranscodeJob {
	int id;
	int argc;
	char** argv;
	pthread_t tid;
	pthread_mutex_t* lock;


    struct InputStream **input_streams;
    int        nb_input_streams;
    struct InputFile   **input_files;
    int        nb_input_files;

    struct OutputStream **output_streams;
    int         nb_output_streams;
    struct OutputFile   **output_files;
    int         nb_output_files;

    struct FilterGraph **filtergraphs;
    int        nb_filtergraphs;

    struct SwsContext *sws_opts;
    struct AVDictionary *swr_opts;
    struct AVDictionary *format_opts, *codec_opts, *resample_opts;

//    FILE* log_file;
//    int log_level;
} TranscodeJob;


extern int avtranscode_init(const char* log_info);

extern void avtranscode_deinit();

extern TranscodeJob* avtranscode_create_job();

extern void avtranscode_destroy_job(TranscodeJob* job);

extern int get_metadata(const char *inputFile, AVMetadata *metadata);

extern int core_main(int argc, char **argv, TranscodeJob* job);



#endif // TRANSCODE_H
