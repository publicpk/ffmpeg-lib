package main;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TranscoderTest {

	@Before
	public void setUp() throws Exception {
		TranscoderWrapper.init(null);
	}

	@Test
	public void testCrop() {
		Transcoder t = new Transcoder(0);
		assertTrue(t.open());
		
		String videoCodec = "h264";
		String audioCodec = "aac";
		String containerFormat = "mp4";
		String basedir = "./";
		String inputFile = basedir + "input1.mp4";
		int[] cropAreas = {
				0,0,1000,720,0, 
				1000,0,280,720,45
				};
		String[] outputFiles = {basedir + "testCrop1.mp4", basedir + "testCrop2.mp4"}; 
		String jobId = "1";
		int res = t.crop(inputFile, videoCodec, audioCodec, containerFormat, cropAreas, outputFiles, jobId);
		t.close();
		
		assertEquals("must be 0", 0, res);
	}

	@Test
	public void testTranscode() {
		//fail("Not yet implemented");
	}

	@Test
	public void testScale() {
		//fail("Not yet implemented");
	}

	@Test
	public void testGetMetadata() {
		//fail("Not yet implemented");
	}

}
