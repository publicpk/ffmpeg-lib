package main;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TranscoderWrapperTest {
	public final static String logFile = "test-%t.log";
	public final static int logLevel = 24; // 48: debug, 40: verbose, 32: info, 24: warning, 16: error, 8: fatal, 0: panic, -8: quiet
	public final static String _logInfo = "file=" + logFile + ":level=" + logLevel;

	@Before
	public void setUp() throws Exception {
		TranscoderWrapper.init(_logInfo);
	}
	
	@After
	public void destroy() throws Exception {
		TranscoderWrapper.deinit();
	}
	
	//@Test
	public void testCrop() {
		TranscoderWrapper t = new TranscoderWrapper();
		
		String videoCodec = "h264";
		String audioCodec = "aac";
		String containerFormat = "mp4";
		String basedir = "./";
		String inputFile = basedir + "input1.mp4";
		int[] cropAreas = {
				0,0,1000,720,0, 
				1000,0,280,720,45
				};
		String[] outputFiles = {basedir + "testCrop1.mp4", basedir + "testCrop2.mp4"}; 
		String jobId = "1";
		int res = t.crop(inputFile, videoCodec, audioCodec, containerFormat, cropAreas, outputFiles, jobId);
		
		assertEquals("result code must be 0", 0, res);
	}
	
	@Test
	public void testCropThread() {
		final int threadCount = 1;
		
		final String videoCodec = "h264";
		final String audioCodec = "aac";
		final String containerFormat = "mp4";
		final String basedir = "./";
		final String baseName = "testCropThread-";
		final String inputFile = basedir + "input1.mp4";
		
		Thread threads[] = new Thread[threadCount];
		for ( int i = 0; i < threadCount; i ++ ) {
			final int id = i;
			threads[i] = new Thread(new Runnable() {
				public void run() {
					TranscoderWrapper t = new TranscoderWrapper();
					int xpos = (id * 100) % 500;
					int ypos = ((id * 100) / 500) * 100;
					int[] cropAreas = {
							xpos,ypos,200,200,0, 
							};
					String[] outputFiles = {basedir + baseName + id + ".mp4"}; 
					String jobId = baseName + id + ".mp4";
					int res = t.crop(inputFile, videoCodec, audioCodec, containerFormat, cropAreas, outputFiles, jobId);
					
					assertEquals("result code must be 0", 0, res);
				}
			});
			threads[i].start();
		}
		for ( int i = 0; i < threadCount; i ++ ) {
			try {
			threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}		
	}
	
	//@Test
	public void testCropThreadWithOneInstance() {
		final int threadCount = 5;
		
		final String videoCodec = "h264";
		final String audioCodec = "aac";
		final String containerFormat = "mp4";
		final String basedir = "./";
		final String baseName = "testCropThreadWithOneInstance-";
		final String inputFile = basedir + "input1.mp4";
		final TranscoderWrapper t = new TranscoderWrapper();
		
		Thread threads[] = new Thread[threadCount];
		for ( int i = 0; i < threadCount; i ++ ) {
			final int id = i;
			threads[i] = new Thread(new Runnable() {
				public void run() {
					assertTrue(t.open());
					int xpos = (id * 100) % 500;
					int ypos = ((id * 100) / 500) * 100;
					int[] cropAreas = {
							xpos,ypos,200,200,0, 
							};
					String[] outputFiles = {basedir + baseName + id + ".mp4"}; 
					String jobId = baseName + id + ".mp4";
					int res = t.crop(inputFile, videoCodec, audioCodec, containerFormat, cropAreas, outputFiles, jobId);
					t.close();
					assertEquals("result code must be 0", 0, res);
				}
			});
			threads[i].start();
		}
		for ( int i = 0; i < threadCount; i ++ ) {
			try {
			threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Test
	public void testTranscode() {
		//fail("Not yet implemented");
	}

	@Test
	public void testScale() {
		//fail("Not yet implemented");
	}
	
}
