package main;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import main.define.LibraryDefine;
import main.define.ServerDefine;
import main.network.DispatchNetwork;
import main.network.ImpNetwork;
import main.util.FileUtil;
import main.util.HttpUtil;
import main.util.StringUtil;

public class TranscoderWrapper {
	public static final long serialVersionUID = main.codec.cls.LoaderUtil.serialVersionUID;

	public static String logFolder = "./log";
	public final static String logFile = logFolder + "/%p-%t.log";
	public final static int logLevel = 48; // 48: debug, 40: verbose, 32: info, 24: warning, 16: error, 8: fatal, 0: panic, -8: quiet
	public final static String _logInfo = "file=" + logFile + ":level=" + logLevel;
	public final int CROP_MINWIDTH = 100;
	public final int CROP_MINHEIGHT = 100;

	final static String regex = "[^\\s\"']+|\"([^\"]*)\"|'([^']*)'"; //"\\s+(?=([^\"]*\"[^\"]*\")*[^\"]*$)"; 
	final static Pattern pattern = Pattern.compile(regex);
	
	Transcoder _native;
	
	static ArrayList<String> m_oCommandList = new ArrayList<String>();
	
	public static void init(String logInfo) {
		File oFile = new File(logFolder);
		
		if(oFile == null || oFile.exists() == false) {
			oFile.mkdir();
		}
		
		if ( logInfo == null )
			logInfo = _logInfo;
		Transcoder.init(logInfo);
	}
	
	public static void init() {
		init(_logInfo);
	}
	
	public static void deinit() {
		Transcoder.deinit();
	}
	
	private static void addCommand(String a_sJobId) {
		if(m_oCommandList == null) {
			m_oCommandList = new ArrayList<String>();
		}
		
		System.out.println("addCommand before size = " + m_oCommandList.size());
		if(m_oCommandList.size() <= 0){
//			init();
		}
		
		m_oCommandList.add(a_sJobId);
		
		System.out.println("addCommand after size = " + m_oCommandList.size());
	}
	
	private static void removeCommand(String a_sJobId) {
		if(m_oCommandList == null) {
			return ;
		}
		
		System.out.println("removeCommand before size = " + m_oCommandList.size());
		
		m_oCommandList.remove(a_sJobId);
		
		System.out.println("removeCommand after size = " + m_oCommandList.size());
		
		if(m_oCommandList.size() <= 0){
//			deinit();
		}
	}
	
	public TranscoderWrapper() {
		_native = new Transcoder(serialVersionUID);
	}
	
	static String[] parseArgString(String cmd) {
		Matcher matcher = pattern.matcher(cmd);
		List<String> argList = new ArrayList<String>();
		while ( matcher.find() ) {
		    if (matcher.group(1) != null) {
		        // Add double-quoted string without the quotes
		    	argList.add(matcher.group(1));
		    } else if (matcher.group(2) != null) {
		        // Add single-quoted string without the quotes
		    	argList.add(matcher.group(2));
		    } else {
		        // Add unquoted word
		    	argList.add(matcher.group());
		    }
		}
		return argList.toArray(new String[0]);
	}
	
	public void test(int val) {
		_native.test(val);
	}
	
	public boolean open() {
		return _native.open();
	}
	
	public void close() {
		_native.close();
	}
	
	private boolean isSupportVidoeCodec(String a_sCodec){
		String[] arsSupportVidoeCodec = {"h264"};
		if(StringUtil.isValid(a_sCodec) == true){
			for(String sSupportCodec : arsSupportVidoeCodec) {
				if(sSupportCodec.equalsIgnoreCase(a_sCodec) == true){
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean isSupportAudioCodec(String a_sCodec){
		String[] arsSupportAudioCodec = {"aac"};
		if(StringUtil.isValid(a_sCodec) == true){
			for(String sSupportCodec : arsSupportAudioCodec) {
				if(sSupportCodec.equalsIgnoreCase(a_sCodec) == true){
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean isSupportContainerFormat(String a_sFormat){
		String[] arsSupportContainerFormat = {"mp4"};
		if(StringUtil.isValid(a_sFormat) == true){
			for(String sSupportFormat : arsSupportContainerFormat) {
				if(sSupportFormat.equalsIgnoreCase(a_sFormat) == true){
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean isSupportCropSize(int width, int height){
		if(width >= CROP_MINWIDTH && height >= CROP_MINHEIGHT){
			return true;
		}
		
		return false;
	}
	
	private boolean isCropAreas(AVMetadata avMetadata, int x, int y, int width, int height, int rotation){
		//rotation is not support, but parameter add
		int nDisEndX = x + width;
		int nDisEndY = y + height;
		
		if((x >= 0 && avMetadata.width >= nDisEndX) && (y >=0 && avMetadata.height >= nDisEndY)){
			return true;
		}
		
		return false;
	}

	public int crop(final String inputFile, final String videoCodec, final String audioCodec, 
			final String containerFormat, final int[] cropAreas, final String[] outputFiles, final String jobId) {

		Thread oThread = new Thread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				TranscoderWrapper.addCommand(jobId);
				
				final TranscoderResult oResult = new TranscoderResult();
				oResult.m_nResultCode = LibraryDefine.RET_CODE_COMMAND_SUCCESS;
				
				while(true)
				{
					// TODO
					//check empty inputfile
					if(StringUtil.isValid(inputFile) == false)
					{
						oResult.m_nResultCode = LibraryDefine.RET_CODE_ERROR_EMPTY_PARAM;
						oResult.m_nResultMsg = LibraryDefine.RET_MSG_ERROR_EMPTY_PARAM_INPUTFILE;
						break;
					}
					
					//check empty video codec
					if(StringUtil.isValid(videoCodec) == false)
					{
						oResult.m_nResultCode = LibraryDefine.RET_CODE_ERROR_EMPTY_PARAM;
						oResult.m_nResultMsg = LibraryDefine.RET_MSG_ERROR_EMPTY_PARAM_VIDEOCODEC;
						break;
					}
					
					//check empty audio codec
					if(StringUtil.isValid(audioCodec) == false)
					{
						oResult.m_nResultCode = LibraryDefine.RET_CODE_ERROR_EMPTY_PARAM;
						oResult.m_nResultMsg = LibraryDefine.RET_MSG_ERROR_EMPTY_PARAM_AUDIOCODEC;
						break;
					}
					
					//check empty container format
					if(StringUtil.isValid(containerFormat) == false)
					{
						oResult.m_nResultCode = LibraryDefine.RET_CODE_ERROR_EMPTY_PARAM;
						oResult.m_nResultMsg = LibraryDefine.RET_MSG_ERROR_EMPTY_PARAM_CONTAINERFORMAT;
						break;
					}
					
					//check empty jobId
					if(StringUtil.isValid(jobId) == false)
					{
						oResult.m_nResultCode = LibraryDefine.RET_CODE_ERROR_EMPTY_PARAM;
						oResult.m_nResultMsg = LibraryDefine.RET_MSG_ERROR_EMPTY_PARAM_JOBID;
						break;
					}
					
					//check empty cropAreas
					if(cropAreas == null)
					{
						oResult.m_nResultCode = LibraryDefine.RET_CODE_ERROR_EMPTY_PARAM;
						oResult.m_nResultMsg = LibraryDefine.RET_MSG_ERROR_EMPTY_PARAM_CROPAREAS;
						break;
					}
					
					//check empty outputFiles
					if(outputFiles == null) 
					{
						oResult.m_nResultCode = LibraryDefine.RET_CODE_ERROR_EMPTY_PARAM;
						oResult.m_nResultMsg = LibraryDefine.RET_MSG_ERROR_EMPTY_PARAM_OUTPUTFILES;
						break;
					}
					
					oResult.m_nResultCode = LibraryDefine.RET_CODE_COMMAND_SUCCESS;

					//check empty each outputFile
					int nIndex = 1;
					for(String output : outputFiles)
					{
						if(StringUtil.isValid(output) == false)
						{
							oResult.m_nResultCode = LibraryDefine.RET_CODE_ERROR_EMPTY_PARAM;
							oResult.m_nResultMsg = String.format(LibraryDefine.RET_MSG_ERROR_EMPTY_PARAM_OUTPUTFILES_EACH, nIndex);
							break;
						}
						
						nIndex++;
					}
					if(oResult.m_nResultCode == LibraryDefine.RET_CODE_ERROR_EMPTY_PARAM)
					{
						break;
					}
					
					//check video Codec not support --> return RET_ERROR_INVALID_PARAM
					if(isSupportVidoeCodec(videoCodec) == false)
					{
						oResult.m_nResultCode = LibraryDefine.RET_CODE_ERROR_INVALID_PARAM;
						oResult.m_nResultMsg = String.format(LibraryDefine.RET_MSG_ERROR_INVALID_PARAM_VIDEOCODEC, videoCodec);
						break;
					}
					
					//check audio Codec not support --> return RET_ERROR_INVALID_PARAM
					if(isSupportAudioCodec(audioCodec) == false)
					{
						oResult.m_nResultCode = LibraryDefine.RET_CODE_ERROR_INVALID_PARAM;
						oResult.m_nResultMsg = String.format(LibraryDefine.RET_MSG_ERROR_INVALID_PARAM_AUDIOCODEC, audioCodec);
						break;
					}
					
					//check container format not support;
					if(isSupportContainerFormat(containerFormat) == false)
					{
						oResult.m_nResultCode = LibraryDefine.RET_CODE_ERROR_INVALID_PARAM;
						oResult.m_nResultMsg = String.format(LibraryDefine.RET_MSG_ERROR_INVALID_PARAM_CONTAINERFORMAT, containerFormat);
						break;
					}
					
					int outputCount = outputFiles.length;
					int cropAreasCount = cropAreas.length;
					if (cropAreasCount != (outputCount * 5) ) 
					{
						// invalid crop area information
						oResult.m_nResultCode = LibraryDefine.RET_CODE_ERROR_INVALID_PARAM;
						oResult.m_nResultMsg = LibraryDefine.RET_MSG_ERROR_INVALID_PARAM_NOTMATCH_CROPAREAS_OUTPUTFILES;
						break;
					}
					
					if(FileUtil.isExists(inputFile) == false)
					{
						oResult.m_nResultCode = LibraryDefine.RET_CODE_ERROR_INVALID_PARAM;
						oResult.m_nResultMsg = String.format(LibraryDefine.RET_MSG_ERROR_INVALID_PARAM_NOTEXIST_INPUTFILE, inputFile);
						break;
					}
					
					AVMetadata metadata = new AVMetadata();
					oResult.m_nResultCode = _native.getMetadata(inputFile, metadata);
					
					System.out.println("Width x Height: " + metadata.width + " x " + metadata.height);
					
					if ( oResult.m_nResultCode < 0 || metadata.hasVideoStream == -1 ) 
					{
						// invalid input file
						oResult.m_nResultCode = LibraryDefine.RET_CODE_ERROR_INVALID_PARAM;
						oResult.m_nResultMsg = String.format(LibraryDefine.RET_MSG_ERROR_INVALID_PARAM_INPUTFILE, inputFile);
						break;
					}
					
					for ( int iOutput = 0; iOutput < outputCount; iOutput++ ) 
					{
						String filter;
						int rotate = 0;
						int x = cropAreas[iOutput*5+0];
						int y = cropAreas[iOutput*5+1];
						int width = cropAreas[iOutput*5+2];
						int height = cropAreas[iOutput*5+3];
						
						// TODO
						//check support crop size 
						if(isSupportCropSize(width, height) == false)
						{
							oResult.m_nResultCode = LibraryDefine.RET_CODE_ERROR_INVALID_PARAM;
							oResult.m_nResultMsg = String.format(LibraryDefine.RET_MSG_ERROR_INVALID_PARAM_CROPSIZE, width, height);
							break;
						}
						
						// check cropAreas values using metadata
						// x, y, width, height, rotation
						if(isCropAreas(metadata, x, y, width, height, rotate) == false)
						{
							oResult.m_nResultCode = LibraryDefine.RET_CODE_ERROR_INVALID_PARAM;
							oResult.m_nResultMsg = LibraryDefine.RET_MSG_ERROR_INVALID_PARAM_CROPAREAS;
							break;
						}
					}
					if(oResult.m_nResultCode == LibraryDefine.RET_CODE_ERROR_INVALID_PARAM)
					{
						break;
					}
					
					String filter_complex = "";
					String output = "";
					int rotateId = 0;
					
					oResult.m_nResultCode = LibraryDefine.RET_CODE_COMMAND_SUCCESS;
					
					for ( int iOutput = 0; iOutput < outputCount; iOutput++ ) 
					{
						String filter;
						int rotate = 0;//cropAreas[iOutput*5+4];
						int x = cropAreas[iOutput*5+0];
						int y = cropAreas[iOutput*5+1];
						int width = cropAreas[iOutput*5+2];
						int height = cropAreas[iOutput*5+3];
						
						if ( rotate != 0 ) 
						{
							filter = String.format(
									"[0:v] rotate=%d*PI/180:c=black [rotated_%d]; " +
									"[rotated_%d] crop=%d:%d:%d:%d [croped_%d]",
									rotate,						// rotation degree
									rotateId,					// rotated buffer id
									rotateId,					//
									width,		// width
									height,		// height
									x,		// x
									y,		// y
									iOutput						// id
									);
						}
						else 
						{
							filter = String.format(
									"[0:v] crop=%d:%d:%d:%d [croped_%d]",
									width,		// width
									height,		// height
									x,		// x
									y,		// y
									iOutput						// id
									);
						}
						
						if ( iOutput+1 != outputCount ) 
						{
							filter += "; "; // has more crop
						}
						filter_complex += filter;

						String out;
						//if output file multi, first file add audio --> spec not define --> dispatch comment
//						if ( iOutput != 0 )
						if ( iOutput == 0 )
						{
							out = String.format(
								"-map \"[croped_%d]\" -c:v libx264 %s ", 
								iOutput, 					// id
								outputFiles[iOutput]
								);
						}
						else 
						{
							// add audio to the first output
							out = String.format(
									"-map \"[croped_%d]\" -map 0:a -c:a libfdk_aac -c:v libx264 %s ", 
									iOutput, 					// id
									outputFiles[iOutput]
									);
						}
						output += out;
					}

					String cmd = String.format("crop -y"
							+ " -i %s"
							+ " -filter_complex \"%s\""
							+ " %s", 
							inputFile,
							filter_complex,
							output
							);
					
//					System.out.println("cmd = " + cmd);
					
					String[] argv = parseArgString(cmd);
					int argc = argv.length;
					
//					System.out.println("Command: argc: " + argc + ", cmd: "+ cmd);
//					for ( int i = 0; i < argc; i ++ ) {
//						System.out.println(i + ":" + argv[i] + "<<<");
//					}
					
					if(oResult.m_nResultCode == LibraryDefine.RET_CODE_COMMAND_SUCCESS)
					{
						oResult.m_nResultCode = LibraryDefine.RET_CODE_ERROR_CROPPING;
						oResult.m_nResultMsg = LibraryDefine.RET_MSG_ERROR_LIBRARY_INTERNAL;
						
						if(open() == true)
						{
							oResult.m_nResultCode = _native.process(argc, argv);
							close();
							System.out.println("process ret: " + oResult.m_nResultCode + ", jobId : " + jobId);
							
							if(oResult.m_nResultCode == 0){
								oResult.m_nResultCode = LibraryDefine.RET_CODE_SUCCESS;
								oResult.m_nResultMsg = LibraryDefine.RET_MSG_SUCCESS;
							}
						}
					}
					
					break;
				}
				
				//TODO server SocketException send result
				DispatchNetwork.request(new ImpNetwork() {
					@Override
					public HashMap<String, String> getRequestParam() {
						HashMap<String, String> hmParam = new HashMap<String, String>();
						
						String sSEcYn = ServerDefine.SEC_N_VALUE;
						if(oResult.m_nResultCode == LibraryDefine.RET_CODE_SUCCESS) {
							sSEcYn = ServerDefine.SEC_Y_VALUE;
						}
						
						hmParam.put(ServerDefine.CMD_PARAM, ServerDefine.CMD_VALUE);
						hmParam.put(ServerDefine.API_PARAM, ServerDefine.API_CROPING_VALUE);
						hmParam.put(ServerDefine.EVT_NO_PARAM, jobId);
						hmParam.put(ServerDefine.SEC_YN_PARAM, sSEcYn);
						hmParam.put(ServerDefine.ERR_CODE_PARAM, oResult.m_nResultCode + "");
						hmParam.put(ServerDefine.ERR_MSG_PARAM, oResult.m_nResultMsg);
						return hmParam;
					}
					
					@Override
					public String getPageUrl() {
						return ServerDefine.SERVER_URL + ServerDefine.CROPING_PAGE;
					}
					
					@Override
					public String getMethod() {
						return HttpUtil.POST;
					}
				});
				
				TranscoderWrapper.removeCommand(jobId);
			}
		});
		oThread.start();
		
		return LibraryDefine.RET_CODE_COMMAND_SUCCESS;
	}
	
	public int transcode(String inputFile, String outputFile, 
			String videoCodec, String audioCodec, String containerFormat, String jobId) {
		return _native.transcode(inputFile, outputFile, videoCodec, audioCodec, containerFormat, jobId);
	}
	
	public int scale(String inputFile, String outputFile, 
			String videoCodec, String audioCodec, String containerFormat, int videoWidth, 
			int videoHeight, String jobId) {
		return _native.scale(inputFile, outputFile, videoCodec, audioCodec, containerFormat, videoWidth, videoHeight, jobId);
	}

//	public static void main(String args[]) 
//	{
//		TranscoderWrapper.init();
//		while(true)
//		{
//			boolean bIsExit = false;
//			System.out.println("\n\n\n****************Transcoder Test****************\n");
//			System.out.println("exit program : 0");
//			
//			System.out.println("default Test : 1");
//				
//			System.out.println("input_file param null Test : 2");
//			System.out.println("videoCodec param null Test : 3");
//			System.out.println("audioCodec param null Test : 4");
//			System.out.println("containerFormat param null Test : 5");
//			System.out.println("cropAreas param null Test : 6");
//			System.out.println("outputFiles param null Test1 : 7");
//			System.out.println("outputFiles param null Test2 : 8");
//			System.out.println("jobId param null Test : 9");
//		
//			System.out.println("videoCodec not support Test : 10");
//			System.out.println("audioCodec not support Test : 11");
//			System.out.println("containerFormat not support Test : 12");
//			System.out.println("cropAreas outputFiles check Test : 13");
//			System.out.println("cropAreas width, height not support Test : 14");
//			System.out.println("cropAreas area over check Test: 15");
//			
//			System.out.println("output Two Create Test : 16");
//			System.out.println("multiThread Test : 17");
//		
//			System.out.println("\nSelect Menu");
//		
//			Scanner sc = new Scanner(System.in);
//			int menu = sc.nextInt();
//		
//			switch(menu)
//			{
//			case 0:
//				bIsExit = true;
//				break;
//			case 1:
//				defaultTest();
//				break;
//			case 2:
//				inputfileparamnullTest();
//				break;
//			case 3:
//				videoCodecparamnullTest();
//				break;
//			case 4:
//				audioCodecparamnullTest();
//				break;
//			case 5:
//				containerFormatparamnullTest();
//				break;
//			case 6:
//				cropAreasparamnullTest();
//				break;
//			case 7:
//				outputFilesparamnullTest1();
//				break;
//			case 8:
//				outputFilesparamnullTest2();
//				break;
//			case 9:
//				jobIdparamnullTest();
//				break;
//			case 10:
//				videoCodecnotsupportTest();
//				break;
//			case 11:
//				audioCodecnotsupportTest();
//				break;
//			case 12:
//				containerFormatnotsupportTest();
//				break;
//			case 13:
//				cropAreasoutputFilescheckTest();
//				break;
//			case 14:
//				cropAreaswidthheightnotsupportTest();
//				break;
//			case 15:
//				cropAreasareaovercheckTest();
//				break;
//			case 16:
//				outputTwoCreateTest();
//				break;
//			case 17:
//				multiThreadTest(10);
//				break;
//			}
//			
//			if(bIsExit == true)
//			{
//				System.out.println("exit program !!!");
//				break;
//			}
//		}
//		
//		TranscoderWrapper.deinit();
//	}
//	
//	static private void multiThreadTest(){
//		final String videoCodec = "h264";
//		final String audioCodec = "aac";
//		final String containerFormat = "mp4";
//		final String basedir = "./";
//		
//		Thread oThread = new Thread(new Runnable() {
//			@Override
//			public void run() {
//				TranscoderWrapper t = new TranscoderWrapper();
//				String inputFile = basedir + "input1.mp4";
//				int[] cropAreas = {0,0,200,720,0};
//				String[] outputFiles = {basedir + "output1.mp4"}; 
//				String jobId = "1";
//				int res = t.crop(inputFile, videoCodec, audioCodec, containerFormat, cropAreas, outputFiles, jobId);
//				System.out.println("First RESULT: " + res);
//			}
//		});
//		
//		oThread.start();
//		
//		Thread oThread2 = new Thread(new Runnable() {
//			@Override
//			public void run() {
//				TranscoderWrapper t2 = new TranscoderWrapper();
////				t2.open();
//				String inputFile1 = basedir + "input2.mp4";
//				int[] cropAreas1 = {100,100,400,360,0};
//				String[] outputFiles1 = {basedir + "output2.mp4"}; 
//				String jobId1 = "13";
//				int res = t2.crop(inputFile1, videoCodec, audioCodec, containerFormat, cropAreas1, outputFiles1, jobId1);
//				System.out.println("Second RESULT: " + res);
//			}
//		});
//		
//		oThread2.start();
//		
//		Thread oThread3 = new Thread(new Runnable() {
//			@Override
//			public void run() {
//				TranscoderWrapper t3 = new TranscoderWrapper();
//				String inputFile1 = basedir + "input3.mp4";
//				int[] cropAreas1 = {100,100,700,460,0};
//				String[] outputFiles1 = {basedir + "output3.mp4"}; 
//				String jobId1 = "13";
//				int res = t3.crop(inputFile1, videoCodec, audioCodec, containerFormat, cropAreas1, outputFiles1, jobId1);
//				System.out.println("Th RESULT: " + res);
//			}
//		});
//		
//		oThread3.start();
//		
//		Thread oThread4 = new Thread(new Runnable() {
//			@Override
//			public void run() {
//				TranscoderWrapper t4 = new TranscoderWrapper();
//				String inputFile1 = basedir + "input4.mp4";
//				int[] cropAreas1 = {100,100,200,560,0};
//				String[] outputFiles1 = {null}; 
//				String jobId1 = "13";
//				int res = t4.crop(inputFile1, videoCodec, audioCodec, containerFormat, cropAreas1, outputFiles1, jobId1);
//				System.out.println("four RESULT: " + res);
//			}
//		});
//		
//		oThread4.start();
//	}
//	
//	static private void defaultTest()
//	{
//		System.out.println("defaultTest start");
//		
//		String videoCodec = "h264";
//		String audioCodec = "aac";
//		String containerFormat = "mp4";
//		String basedir = "./";
//		
//		TranscoderWrapper t = new TranscoderWrapper();
//		String inputFile = basedir + "input1.mp4";
//		int[] cropAreas = {0,0,200,720,0};
//		String[] outputFiles = {basedir + "output1.mp4"}; 
//		String jobId = "1";
//		int res = t.crop(inputFile, videoCodec, audioCodec, containerFormat, cropAreas, outputFiles, jobId);
//	}
//	
//	static private void inputfileparamnullTest()
//	{
//		System.out.println("inputfileparamnullTest start");
//		
//		String videoCodec = "h264";
//		String audioCodec = "aac";
//		String containerFormat = "mp4";
//		String basedir = "./";
//		
//		TranscoderWrapper t = new TranscoderWrapper();
//		String inputFile = null;
//		int[] cropAreas = {0,0,200,720,0};
//		String[] outputFiles = {basedir + "output1.mp4"}; 
//		String jobId = "1";
//		int res = t.crop(inputFile, videoCodec, audioCodec, containerFormat, cropAreas, outputFiles, jobId);
//	}
//	
//	static private void videoCodecparamnullTest()
//	{
//		System.out.println("videoCodecparamnullTest start");
//		
//		String videoCodec = null;
//		String audioCodec = "aac";
//		String containerFormat = "mp4";
//		String basedir = "./";
//		
//		TranscoderWrapper t = new TranscoderWrapper();
//		String inputFile = basedir + "input1.mp4";
//		int[] cropAreas = {0,0,200,720,0};
//		String[] outputFiles = {basedir + "output1.mp4"}; 
//		String jobId = "1";
//		int res = t.crop(inputFile, videoCodec, audioCodec, containerFormat, cropAreas, outputFiles, jobId);
//	}
//	
//	static private void audioCodecparamnullTest()
//	{
//		System.out.println("audioCodecparamnullTest start");
//		
//		String videoCodec = "h264";
//		String audioCodec = null;
//		String containerFormat = "mp4";
//		String basedir = "./";
//		
//		TranscoderWrapper t = new TranscoderWrapper();
//		String inputFile = basedir + "input1.mp4";
//		int[] cropAreas = {0,0,200,720,0};
//		String[] outputFiles = {basedir + "output1.mp4"}; 
//		String jobId = "1";
//		int res = t.crop(inputFile, videoCodec, audioCodec, containerFormat, cropAreas, outputFiles, jobId);
//	}
//	
//	static private void containerFormatparamnullTest()
//	{
//		System.out.println("containerFormatparamnullTest start");
//		
//		String videoCodec = "h264";
//		String audioCodec = "aac";
//		String containerFormat = null;
//		String basedir = "./";
//		
//		TranscoderWrapper t = new TranscoderWrapper();
//		String inputFile = basedir + "input1.mp4";
//		int[] cropAreas = {0,0,200,720,0};
//		String[] outputFiles = {basedir + "output1.mp4"}; 
//		String jobId = "1";
//		int res = t.crop(inputFile, videoCodec, audioCodec, containerFormat, cropAreas, outputFiles, jobId);
//	}
//	
//	static private void cropAreasparamnullTest()
//	{
//		System.out.println("cropAreasparamnullTest start");
//		
//		String videoCodec = "h264";
//		String audioCodec = "aac";
//		String containerFormat = "mp4";
//		String basedir = "./";
//		
//		TranscoderWrapper t = new TranscoderWrapper();
//		String inputFile = basedir + "input1.mp4";
//		int[] cropAreas = null;
//		String[] outputFiles = {basedir + "output1.mp4"}; 
//		String jobId = "1";
//		int res = t.crop(inputFile, videoCodec, audioCodec, containerFormat, cropAreas, outputFiles, jobId);
//	}
//	
//	static private void outputFilesparamnullTest1()
//	{
//		System.out.println("outputFilesparamnullTest1 start");
//		
//		String videoCodec = "h264";
//		String audioCodec = "aac";
//		String containerFormat = "mp4";
//		String basedir = "./";
//		
//		TranscoderWrapper t = new TranscoderWrapper();
//		String inputFile = basedir + "input1.mp4";
//		int[] cropAreas = {0,0,200,720,0};
//		String[] outputFiles = null; 
//		String jobId = "1";
//		int res = t.crop(inputFile, videoCodec, audioCodec, containerFormat, cropAreas, outputFiles, jobId);
//	}
//	
//	static private void outputFilesparamnullTest2()
//	{
//		System.out.println("outputFilesparamnullTest2 start");
//		
//		String videoCodec = "h264";
//		String audioCodec = "aac";
//		String containerFormat = "mp4";
//		String basedir = "./";
//		
//		TranscoderWrapper t = new TranscoderWrapper();
//		String inputFile = basedir + "input1.mp4";
//		int[] cropAreas = {100,0,200,720,0,   0,0,200,720,0};
//		String[] outputFiles = {basedir + "output1.mp4", null}; 
//		String jobId = "1";
//		int res = t.crop(inputFile, videoCodec, audioCodec, containerFormat, cropAreas, outputFiles, jobId);
//	}
//	
//	static private void jobIdparamnullTest()
//	{
//		System.out.println("jobIdparamnullTest");
//		
//		String videoCodec = "h264";
//		String audioCodec = "aac";
//		String containerFormat = "mp4";
//		String basedir = "./";
//		
//		TranscoderWrapper t = new TranscoderWrapper();
//		String inputFile = basedir + "input1.mp4";
//		int[] cropAreas = {0,0,200,720,0};
//		String[] outputFiles = {basedir + "output1.mp4"}; 
//		String jobId = null;
//		int res = t.crop(inputFile, videoCodec, audioCodec, containerFormat, cropAreas, outputFiles, jobId);
//	}
//	
//	static private void videoCodecnotsupportTest()
//	{
//		System.out.println("videoCodecnotsupportTest start");
//		
//		String videoCodec = "mpeg4";
//		String audioCodec = "aac";
//		String containerFormat = "mp4";
//		String basedir = "./";
//		
//		TranscoderWrapper t = new TranscoderWrapper();
//		String inputFile = basedir + "input1.mp4";
//		int[] cropAreas = {0,0,200,720,0};
//		String[] outputFiles = {basedir + "output1.mp4"}; 
//		String jobId = "1";
//		int res = t.crop(inputFile, videoCodec, audioCodec, containerFormat, cropAreas, outputFiles, jobId);
//	}
//	
//	static private void audioCodecnotsupportTest()
//	{
//		System.out.println("audioCodecnotsupportTest start");
//		
//		String videoCodec = "h264";
//		String audioCodec = "mp3";
//		String containerFormat = "mp4";
//		String basedir = "./";
//		
//		TranscoderWrapper t = new TranscoderWrapper();
//		String inputFile = basedir + "input1.mp4";
//		int[] cropAreas = {0,0,200,720,0};
//		String[] outputFiles = {basedir + "output1.mp4"}; 
//		String jobId = "1";
//		int res = t.crop(inputFile, videoCodec, audioCodec, containerFormat, cropAreas, outputFiles, jobId);
//	}
//	
//	static private void containerFormatnotsupportTest()
//	{
//		System.out.println("containerFormatnotsupportTest start");
//		
//		String videoCodec = "h264";
//		String audioCodec = "aac";
//		String containerFormat = "wav";
//		String basedir = "./";
//		
//		TranscoderWrapper t = new TranscoderWrapper();
//		String inputFile = basedir + "input1.mp4";
//		int[] cropAreas = {0,0,200,720,0};
//		String[] outputFiles = {basedir + "output1.mp4"}; 
//		String jobId = "1";
//		int res = t.crop(inputFile, videoCodec, audioCodec, containerFormat, cropAreas, outputFiles, jobId);
//	}
//	
//	static private void cropAreasoutputFilescheckTest()
//	{
//		System.out.println("cropAreasoutputFilescheckTest start");
//		
//		String videoCodec = "h264";
//		String audioCodec = "aac";
//		String containerFormat = "mp4";
//		String basedir = "./";
//		
//		TranscoderWrapper t = new TranscoderWrapper();
//		String inputFile = basedir + "input1.mp4";
//		int[] cropAreas = {0,0,200,720,0,   0,0,100,200};
//		String[] outputFiles = {basedir + "output1.mp4", basedir + "output2.mp4"}; 
//		String jobId = "1";
//		int res = t.crop(inputFile, videoCodec, audioCodec, containerFormat, cropAreas, outputFiles, jobId);
//	}
//	
//	static private void cropAreaswidthheightnotsupportTest()
//	{
//		System.out.println("cropAreaswidthheightnotsupportTest start");
//		
//		String videoCodec = "h264";
//		String audioCodec = "aac";
//		String containerFormat = "mp4";
//		String basedir = "./";
//		
//		TranscoderWrapper t = new TranscoderWrapper();
//		String inputFile = basedir + "input1.mp4";
//		//test video size 1280* 720
//		int[] cropAreas = {1000,0,50,50,0};
//		String[] outputFiles = {basedir + "output1.mp4"}; 
//		String jobId = "1";
//		int res = t.crop(inputFile, videoCodec, audioCodec, containerFormat, cropAreas, outputFiles, jobId);
//	}
//	
//	
//	static private void cropAreasareaovercheckTest()
//	{
//		System.out.println("cropAreasareaovercheckTest start");
//		
//		String videoCodec = "h264";
//		String audioCodec = "aac";
//		String containerFormat = "mp4";
//		String basedir = "./";
//		
//		TranscoderWrapper t = new TranscoderWrapper();
//		String inputFile = basedir + "input1.mp4";
//		//test video size 1280* 720
//		int[] cropAreas = {1000,0,500,720,0};
//		String[] outputFiles = {basedir + "output1.mp4"}; 
//		String jobId = "1";
//		int res = t.crop(inputFile, videoCodec, audioCodec, containerFormat, cropAreas, outputFiles, jobId);
//	}
//	
//	static private void outputTwoCreateTest()
//	{
//		System.out.println("outputTwoCreateTest start");
//		
//		String videoCodec = "h264";
//		String audioCodec = "aac";
//		String containerFormat = "mp4";
//		String basedir = "./";
//		
//		TranscoderWrapper t = new TranscoderWrapper();
//		String inputFile = basedir + "input1.mp4";
//		int[] cropAreas = {0,0,500,720,0,   0,0,300,200,0};
//		String[] outputFiles = {basedir + "output1.mp4", basedir + "output2.mp4"}; 
//		String jobId = "1";
//		int res = t.crop(inputFile, videoCodec, audioCodec, containerFormat, cropAreas, outputFiles, jobId);
//	}
//		
//	static private void multiThreadTest(final int count)
//	{
//		System.out.println("multiThreadTest");
//		
//		final String videoCodec = "h264";
//		final String audioCodec = "aac";
//		final String containerFormat = "mp4";
//		final String basedir = "./";
//		final String inputFile = basedir + "input1.mp4";
//		
//		for(int i=1; i<count+1; i++) 
//		{
//			final int index = i;
//			Thread oThread = new Thread(new Runnable() 
//			{
//				@Override
//				public void run() 
//				{
//					TranscoderWrapper t1 = new TranscoderWrapper();
//					
//					String[] outputFiles1 = {basedir + "output" + index + ".mp4"}; 
//					String jobId1 = index + "";
//					int width = 1280 / count * index;
//					int height = 720 / count * index;
//					
//					int[] cropAreas1 = {0,0,width,height,0};
//					
//					int res = t1.crop(inputFile, videoCodec, audioCodec, containerFormat, cropAreas1, outputFiles1, jobId1);
//				}
//			});
//			
//			oThread.start();
//		}
//	}
}
