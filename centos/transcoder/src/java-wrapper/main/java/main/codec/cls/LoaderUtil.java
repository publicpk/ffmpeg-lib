package main.codec.cls;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class LoaderUtil {
	public static final long serialVersionUID = 0x4792478298347L;

	private static final int NB_CHAR = 7; 
	
	static {
		//
		// IMPORTANT: Keep the sequence of loading libraries
		// nor you'll get the link error
		// x264,fdk-aac,avutil,swresample,swscale,avcodec,avformat,postproc,avfilter,avdevice
	   long[] uids = new long[12];
	   uids[0] = $classes.$cls01.serialVersionUID;
	   uids[1] = $classes.$cls02.serialVersionUID;
	   uids[2] = $classes.$cls03.serialVersionUID;
	   uids[3] = $classes.$cls04.serialVersionUID;
	   uids[4] = $classes.$cls05.serialVersionUID;
	   uids[5] = $classes.$cls06.serialVersionUID;
	   uids[6] = $classes.$cls07.serialVersionUID;
	   uids[7] = $classes.$cls08.serialVersionUID;
	   uids[8] = $classes.$cls09.serialVersionUID;
	   uids[9] = $classes.$cls10.serialVersionUID;
	   uids[10] = $classes.$cls11.serialVersionUID;
	   uids[11] = $classes.$cls12.serialVersionUID;
	   
	   String[] libs = libnames(uids).split(",");
	   try {
		   for ( int i = 0; i < libs.length; i++ )
			   System.loadLibrary(libs[i]);
			
		   // jni library
		   System.loadLibrary("transcoder");
		   System.loadLibrary("Transcoder-java");
		}
		catch (UnsatisfiedLinkError e) {
			System.err.println("Error while loading module...");
			e.printStackTrace();
			// TODO handle it
			throw e;
		}
	   
		//System.out.println("module's been loaded successfully...");
	}
	
    private static String libnames(long[] codedUid) {
        int len = codedUid.length;
        if(len == 0)
            return "";
        // StringBuilder where we will decompose the hidden String
        StringBuilder sb = new StringBuilder(len * NB_CHAR);
        --len;
        // loop in reverse
        for(int i = len; i >= 0; --i) {
            // until all chars extracted
            long tmp = codedUid[i];
            while(tmp > 0) {
                // extract rightmost char
                char c = (char) (tmp % 0xFF);
                // into the StringBuilder
                sb.insert(0, c);
                tmp /= 0xFF;
            }
        }
        return sb.toString();
    }
	
    /**
     * Loads library from current JAR archive
     * 
     * The file from JAR is copied into system temporary directory and then loaded. The temporary file is deleted after exiting.
     * Method uses String as filename because the pathname is "abstract", not system-dependent.
     * 
     * @param filename The filename inside JAR as absolute path (beginning with '/'), e.g. /package/File.ext
     * @throws IOException If temporary file creation or read/write operation fails
     * @throws IllegalArgumentException If source file (param path) does not exist
     * @throws IllegalArgumentException If the path is not absolute or if the filename is shorter than three characters (restriction of {@see File#createTempFile(java.lang.String, java.lang.String)}).
     */
    public static void loadLibraryFromJar(String path) throws IOException {
 
        if (!path.startsWith("/")) {
            throw new IllegalArgumentException("The path has to be absolute (start with '/').");
        }
 
        // Obtain filename from path
        String[] parts = path.split("/");
        String filename = (parts.length > 1) ? parts[parts.length - 1] : null;
 
        // Split filename to prexif and suffix (extension)
        String prefix = "";
        String suffix = null;
        if (filename != null) {
            parts = filename.split("\\.", 2);
            prefix = parts[0];
            suffix = (parts.length > 1) ? "."+parts[parts.length - 1] : null; // Thanks, davs! :-)
        }
 
        // Check if the filename is okay
        if (filename == null || prefix.length() < 3) {
            throw new IllegalArgumentException("The filename has to be at least 3 characters long.");
        }
 
        // Prepare temporary file
        File temp = File.createTempFile(prefix, suffix);
        temp.deleteOnExit();
 
        if (!temp.exists()) {
            throw new FileNotFoundException("File " + temp.getAbsolutePath() + " does not exist.");
        }
 
        // Prepare buffer for data copying
        byte[] buffer = new byte[1024];
        int readBytes;
 
        // Open and check input stream
        InputStream is = LoaderUtil.class.getResourceAsStream(path);
        if (is == null) {
            throw new FileNotFoundException("File " + path + " was not found inside JAR.");
        }
 
        // Open output stream and copy data between source file in JAR and the temporary file
        OutputStream os = new FileOutputStream(temp);
        try {
            while ((readBytes = is.read(buffer)) != -1) {
                os.write(buffer, 0, readBytes);
            }
        } finally {
            // If read/write fails, close streams safely before throwing an exception
            os.close();
            is.close();
        }
 
        // Finally, load the library
        System.load(temp.getAbsolutePath());
    }    
    
}
