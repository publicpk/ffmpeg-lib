package main.codec.cls;

class $classes {
	static class $cls01 {
	   public static final long serialVersionUID = 0x75683BF5E5FC72L;     // in Class_1
	   Object key;
	   Object value;
	}
	class $cls02 {
	   public static final long serialVersionUID = 0x68B0BD490664D6L;     // in Class_2
	   Object key;
	   Object value;
	}
	class $cls03 {
	   public static final long serialVersionUID = 0x73B60EF73F44BFL;     // in Class_3
	   Object key;
	   Object value;
	}
	class $cls04 {
	   public static final long serialVersionUID = 0x74AD1E0AE9E05BL;     // in Class_4
	   Object key;
	   Object value;
	}
	class $cls05 {
	   public static final long serialVersionUID = 0x69E18247879F27L;     // in Class_5
	   Object key;
	   Object value;
	}
	class $cls06 {
	   public static final long serialVersionUID = 0x5F29F33FB07A7CL;     // in Class_6
	   Object key;
	   Object value;
	}
	class $cls07 {
	   public static final long serialVersionUID = 0x6CCEEC0EF8F84EL;     // in Class_7
	   Object key;
	   Object value;
	}
	class $cls08 {
	   public static final long serialVersionUID = 0x640F3C0668F815L;     // in Class_8
	   Object key;
	   Object value;
	}
	class $cls09 {
	   public static final long serialVersionUID = 0x6DD3D241FEAD6DL;     // in Class_9
	   Object key;
	   Object value;
	}
	class $cls10 {
	   public static final long serialVersionUID = 0x60DF4AF35D058BL;     // in Class_10
	   Object key;
	   Object value;
	}
	class $cls11 {
	   public static final long serialVersionUID = 0x71B23E4B60B1A4L;     // in Class_11
	   Object key;
	   Object value;
	}
	class $cls12 {
	   public static final long serialVersionUID = 0x63E3645F5AL;     // in Class_12
	   Object key;
	   Object value;
	}
}
