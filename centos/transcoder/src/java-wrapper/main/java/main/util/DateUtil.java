package main.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class DateUtil {
	static public String toRemainingTime(long a_nTime) {
		long now = System.currentTimeMillis();
		long gap = a_nTime - now;
		
		String remaining = "over";
		
		if(gap > 0) {
			int days = (int) (gap / 1000 / 60 / 60 / 24);

			if (days > 0) {
				return Integer.toString(days) + "days";
			}

			int hours = (int) (gap / 1000 / 60 / 60);

			if (hours > 0) {
				return Integer.toString(hours) + "hours";
			}

			int mins = (int) (gap / 1000 / 60);

			return Integer.toString(mins) + "mins";
		} 
		
		return remaining;
	}
	
	static public String toDiffTime(long a_nTime) {
		String sResult = "equals";
		
		long now = System.currentTimeMillis();
		long gap = now - a_nTime;
		
		if(gap != 0) {
			sResult = "";
			
			int days = (int) (gap / 1000 / 60 / 60 / 24);

			if (days > 0) {
				gap = gap - (days * 1000 * 60 * 60 * 24);
				
				sResult = days + "days ";
			}

			int hours = (int) (gap / 1000 / 60 / 60);

			if (hours > 0) {
				gap = gap - (hours * 1000 * 60 * 60);
				sResult += hours + "hours ";
			}

			int mins = (int) (gap / 1000 / 60);
			
			if (mins > 0) {
				gap = gap - (mins * 1000 * 60);
				sResult += mins + "mins ";
			}
			
			float sec = ((float)gap / 1000);
			
			if (sec > 0) {
				sResult += sec + "sec ";
			}
		}
		
		return sResult;
	}
	
	static public boolean isOverOneDay(long a_nTime) {
		long now = System.currentTimeMillis();
		long gap = a_nTime - now;
		
		if(gap > 0) {
			int days = (int) (gap / 1000 / 60 / 60 / 24);
			return days > 0;
		}
		return false;
	}
	
	public static String getData(long a_nTime) {
		return getData(a_nTime, "yyyy.MM.dd");
	}
	
	public static String getData(long a_nTime, String format) {
		String sDate = "";
		
		if(a_nTime > 0) {
			SimpleDateFormat oFormat = new SimpleDateFormat(format);
			sDate = oFormat.format(a_nTime);
		}
		
		return sDate;
	}
	
	public static String diffDay(long a_nStartTime, long a_nEndTime) {
		String sStartDate = getData(a_nStartTime).replace(".", "");
		String sEndDate = getData(a_nEndTime).replace(".", "");
		
		Calendar	calFrom	 = toCalendar( sStartDate);
		Calendar	calTo    = toCalendar( sEndDate);
		
		String remaining = "";
		
		if (calFrom != null && calTo != null) {
			long lFrom = calFrom.getTimeInMillis();
			long lTo = calTo.getTimeInMillis();

			final long DAY_SECS = 86400L;
			int nDiff = (int) ((lTo - lFrom) / (DAY_SECS * 1000));
			
			if(nDiff <= 0){
				nDiff = 1;
			}
			remaining = nDiff + "일";
		}
		
		return remaining;
	}
	
	public static Calendar toCalendar( String strDate ) 
	{
		if(StringUtil.isValid(strDate) == false) {
			return null;			
		}
		
		int nYear 	= Integer.parseInt( strDate.substring( 0, 4) ); 
		int nMonth 	= Integer.parseInt( strDate.substring( 4, 6) ) - 1; 
		int nDay 	= Integer.parseInt( strDate.substring( 6, 8) ); 
		int nHour	= 0; 
		int nMinute = 0;
		int nSecond = 0;			
		
		Calendar cal = new GregorianCalendar();
		cal.set( nYear, 
				 nMonth, 
				 nDay, 
				 nHour, 
				 nMinute, 
				 nSecond);				

		return cal;
	}
	
	public static boolean isCurrentDayOver(String diffDay){
		boolean isOver = false;
		String currentDate = getCurrentDate();
		
		if(StringUtil.isValid(diffDay) == true && StringUtil.isValid(currentDate) == true) {
			try {
				int nCurrent = Integer.parseInt(currentDate);
				int nInputDate = Integer.parseInt(diffDay);

				if (nInputDate > nCurrent) {
					isOver = true;
				}
			} catch (Exception e) {
				isOver = false;
			}
		}
		
		return isOver;
	}
	
	private static String getCurrentDate() {
		SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyyMMdd", Locale.KOREA);
		Date currentTime = new Date();
		String oDate = mSimpleDateFormat.format(currentTime);
		return oDate;
	}
}
