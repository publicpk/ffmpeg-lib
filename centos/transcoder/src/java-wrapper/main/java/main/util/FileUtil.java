package main.util;

import java.io.File;

public class FileUtil {
	public static boolean isExists(String a_input) 
	{
		boolean bIsExists = false;
		
		File oFile = new File(a_input);
		
		if(oFile != null || oFile.exists() == true)
		{
			bIsExists = true;
		}
		
		return bIsExists;
	}
}
