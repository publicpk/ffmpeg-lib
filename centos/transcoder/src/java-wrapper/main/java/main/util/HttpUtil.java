package main.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;

public class HttpUtil {
	public static final String GET = "GET";
	public static final String POST = "POST";
	
	public static final int RESULT_FAIL = -1;
	
	public static class HttpResult{
		public HttpResult(){
			m_nResultCode = RESULT_FAIL;
		}
		
		public int m_nResultCode;
		public String m_sResultData;
	}
	
	public HttpResult request(String a_sUrl, String a_sMethod, HashMap<String, String> a_hmParam) {
		HttpURLConnection oHttpURLConnection = null;
		StringBuilder sbRequestData = new StringBuilder();
		OutputStream osRequesetStream = null;
		BufferedReader brResponseData = null;
		StringBuilder sbResponseData = new StringBuilder();
		String sResponseData = null;
		HttpResult oHttpResult = new HttpResult();

		try {
			Iterator<String> keys = a_hmParam.keySet().iterator();
			
			while( keys.hasNext() ){
	            String key = keys.next();
	            sbRequestData.append(key);
	            sbRequestData.append("=");
	            sbRequestData.append(a_hmParam.get(key));
	            
	            if(keys.hasNext() == true) {
	            	sbRequestData.append("&");
	            }
	        }

			URL oUrl = new URL(a_sUrl);
			
			// Set up the initial connection
			oHttpURLConnection = (HttpURLConnection) oUrl.openConnection();
			
			// Set http header info
			oHttpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			oHttpURLConnection.setRequestProperty("Content-Length", "" + Integer.toString(sbRequestData.toString().getBytes().length));
			oHttpURLConnection.setRequestMethod(a_sMethod);
			
			oHttpURLConnection.setDoOutput(true);
			oHttpURLConnection.setDoInput(true);
			oHttpURLConnection.setUseCaches(false);
			oHttpURLConnection.setDefaultUseCaches(false);
			oHttpURLConnection.setReadTimeout(10000);
			
			// get the output stream and write the output to the server
			// not needed in this example
			osRequesetStream = oHttpURLConnection.getOutputStream();
			osRequesetStream.write(sbRequestData.toString().getBytes());
			
			System.out.println("a_sUrl = " + a_sUrl);
			System.out.println("osRequesetStream = " + osRequesetStream);
			
			osRequesetStream.flush();
			osRequesetStream.close();
			
			// read the result from the server
			brResponseData = new BufferedReader(new InputStreamReader(
					oHttpURLConnection.getInputStream()));
			
			while ((sResponseData = brResponseData.readLine()) != null) {
				sbResponseData.append(sResponseData + '\n');
			}
			
			brResponseData.close();
			
			oHttpResult.m_nResultCode = oHttpURLConnection.getResponseCode();
			oHttpResult.m_sResultData = sbResponseData.toString();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			oHttpResult.m_sResultData = e.getMessage();
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			oHttpResult.m_sResultData = e.getMessage();
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			oHttpResult.m_sResultData = e.getMessage();
			e.printStackTrace();
		} finally {
			// close the connection, set all objects to null
			if(oHttpURLConnection != null) {
				oHttpURLConnection.disconnect();
			}
			
			brResponseData = null;
			sbResponseData = null;
			osRequesetStream = null;
			oHttpURLConnection = null;
		}

		return oHttpResult;
	}
}
