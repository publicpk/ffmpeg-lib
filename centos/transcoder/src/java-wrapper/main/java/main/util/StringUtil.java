package main.util;

import java.util.Locale;


/**
 * String 의 값 검사, 변환 등  
 */
public class StringUtil {
	
	/**
	 * null 또는 "" 인지 검사한다. 
	 * @param v 검사 대상 
	 * @return true : null 도 아니고 ""도 아닌 의미 있는 값일경우,  false : null 이나 "" 일 경우  
	 */
	public static boolean isValid(String a_sString)
	{
		return (a_sString!=null&&!"".equals(a_sString));
	}
	
	public static boolean isEmpty(String a_sString)
	{
	    return !isValid(a_sString);
	}
	
	/**
	 * 영어인 경우 소문자 변경
	 * @param 영어 alphabet
	 * @return
	 */
	public static String toEnglishLowerCase(String a_salphabet)
	{
	    if(a_salphabet == null){
	        return "";
	    }
        return a_salphabet.toLowerCase(Locale.ENGLISH);
	}
}

