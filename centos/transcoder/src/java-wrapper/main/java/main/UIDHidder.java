package main;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.awt.*;

/*
 * An application to generate the serialVersionUIDs to encrypt a String in SerialVersionUID
 * Generate also the method code to rebuild it
 */
public class UIDHidder extends JPanel {
	
	private static final long serialVersionUID = 0x1L;
	
	// the number of characters that fit in a long is 8 but to avoid sign problem (when the first one
	// is > 127) we'll store 7 characters in every long
	private static final int NB_CHAR = 7; 

	// to receive the String to encrypt
	private JTextField txtField;
	// to display the code to cut & paste
	private JTextArea codeText;
	// to display the list of the UID
	private MyTable table;
	private MyModel tableModel;
	private JPanel uidPanel;
	// the UID themselves
	private long[] uid;
	// the JLabel to display translation back
	private JLabel labelBack;
	
	UIDHidder() {
		super(new BorderLayout());
		// init uid length to 0
		uid = new long[0];
		
		// the NORTH region where user input text to encrypt
		buildNorth();
		// the CENTER region region contains 2 ScrollPane
		// one with the serailVersionUID the second one with the code to decypher the String
		buildCenter();
		// the JLabel to display back the translated screen
		labelBack = new JLabel("Translated back:");
		labelBack.setForeground(Color.BLUE);
		add(labelBack, BorderLayout.SOUTH);
	}
	
	/*
	 * Build the NORTH region 
	 */
	private void buildNorth() {
		// the NORTH region contains a JTextField with the String to encrypt
		// make a GridLayout(3,1) to put with a blank label in 1st and 3rd row
		// (so won't touch the border)
		JPanel north = new JPanel(new GridLayout(3, 1));
		// first row empty
		north.add(new JLabel(" "));
		// a Box for the center with the JLabel and the JTextField for user input
		Box center = Box.createHorizontalBox();
		JLabel label = new JLabel("  String to encrypt:  ");
		label.setForeground(Color.BLUE);
		center.add(label);
		// where the use input the String to encrypt
		txtField = new JTextField(40);
		txtField.getDocument().addDocumentListener(new EncryptListener());
		center.add(txtField);
		center.add(new JLabel("     "));				// not to touch right border
		// add the Box in the center (middle of the GridLayout)
		north.add(center);
		// 3rd row empty
		north.add(new JLabel(" "));
		add(north, BorderLayout.NORTH);				
	}

	// build the center region
	private void buildCenter() {
		// the CENTER region region contains 2 ScrollPane
		// one with the serailVersionUID the second one with the code to decypher the String
		JPanel centerPanel = new JPanel(new GridLayout(2,1));
		// the top part a JTable in it's scrollpane
		tableModel = new MyModel();
		table = new MyTable(tableModel);
		uidPanel = new JPanel(new BorderLayout());
		uidPanel.add(new JScrollPane(table), BorderLayout.CENTER);
		// just for the JTable to show it's bottom border and to add a margin both sides
		uidPanel.add(new JLabel(" "), BorderLayout.SOUTH);
		uidPanel.add(new JLabel("   "), BorderLayout.WEST);
		uidPanel.add(new JLabel("   "), BorderLayout.EAST);
		centerPanel.add(uidPanel);
		// the bottom part with the TextArea showing the code
		codeText = new JTextArea();
		centerPanel.add(new JScrollPane(codeText));
		add(centerPanel, BorderLayout.CENTER);		
	}
	
	/*
	 * Used to decript back to String an array of serialVersionUID encoded by this program
	 * Yoy will have to cut & paste this method into your production code
	 */
	private String decrypter(long[] codedUid) {
		int len = codedUid.length;
		if(len == 0)
			return "";
		// StringBuilder where we will decompose the hidden STring
		StringBuilder sb = new StringBuilder(len * NB_CHAR);
		--len;
		// loop in reverse
		for(int i = len; i >= 0; --i) {
			// until all chars extracted
			long tmp = codedUid[i];
			while(tmp > 0) {
				// extract rightmost char
				char c = (char) (tmp % 0xFF);
				// into the StringBuilder
				sb.insert(0, c);
				tmp /= 0xFF;
			}
		}
		return sb.toString();
	}
	
	// the standarnd method to start the whole damned thing
	public static void main(String[] args) {
		JFrame frame = new JFrame("Encryption in serialVersionUID");
		frame.add(new UIDHidder());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 600);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
	}

	/*
	 * Extends JTable just to be able to set the cell renderer
	 */
	class MyTable extends JTable {

		private static final long serialVersionUID = 1L;
		// to save the model which is also the renderer
		TableCellRenderer model;
		
		// receives as parameter the TableModel which is also a TableCellRenderer
		MyTable(MyModel model) {
			super(model);
			this.model = model;
		}
		/** tell to call the Model which is also a renderer for all rendering (we return JLabel) */
		public TableCellRenderer getCellRenderer(int row, int col) {
			return model;
		}

	}

	/*
	 * To feed the JTable with the appropriate data
	 * Col: 1 the number of the class
	 * Col: 2 the serialVersionUID 
	 */
	class MyModel extends AbstractTableModel implements TableCellRenderer {

		private static final long serialVersionUID = 0x1L;
		
		// the label that will be displayed in the JTable
		private JLabel label = new JLabel();
		// the table header
		private String[] header = {"Class name", "serialVersionUID"};
		
		// always two column
		public int getColumnCount() {
			return 2;
		}

		// returns the number of serialVersionUID
		public int getRowCount() {
			return uid.length;
		}

		// first column is class number second column the serialVersionUID
		public Object getValueAt(int row, int col) {
			if(uid.length == 0)
				return null;
			String str;
			// first column
			if(col == 0)
				str = "   Class_" + (row + 1);
			else
				str = String.format("0x%XL   ", uid[row]);
			return str;
		}

		// return the 2 column headers
		public String getColumnName(int col) {
			return header[col];
		}

		public Component getTableCellRendererComponent(JTable arg0,
				Object arg1, boolean arg2, boolean arg3, int row, int col) {
			if(arg1 == null) {
				label.setText("");
			}
			else {
				if(col == 0)
					label.setHorizontalAlignment(SwingConstants.CENTER);
				else
					label.setHorizontalAlignment(SwingConstants.RIGHT);				
				label.setText((String) arg1);
			}
			return label;
		}
		
	}
	
	/**
	 * A listener to be informed whenever the JTextField for the string to encode changed
	 */
	class EncryptListener implements DocumentListener {

		// when document changes
		public void changedUpdate(DocumentEvent arg0) {
			// clear the textArea in any case
			codeText.setText("");
			// get the String from the JTextField
			String str = txtField.getText().trim();
			// if empty String ignore the request
			int len = str.length();
			if(len == 0) {
				uid = new long[0];			// for the JTable to know nothing to display
				labelBack.setText("Translated back:");
				return;
			}
			// convert the String to encrypt into char
			char[] digit = str.toCharArray();
			// so we will use NB_CHAR digits from the string to build serialVersionUId
			int nbSerial = digit.length / NB_CHAR;
			// if not an exact multiple of NB_CHAR we need one more
			if(len % NB_CHAR != 0)
				++nbSerial;
			
			uid = new long[nbSerial];
			// cumulate the char in longwords
			for(int i = 0; i < len; ++i) {
				int k = i / NB_CHAR;			// index of the uid
				uid[k] *= 0xFF;
				uid[k] += digit[i];
			}
			// inform table changed
			table.tableChanged(null);
			// display decodes Sring
			labelBack.setText("Translated back: \"" + decrypter(uid) + "\"");
			
			// prepare the txtArea
			StringBuilder sb = new StringBuilder();
			sb.append("   // UID to cut & paste in your different classes\n");
			for(int i = 0; i < nbSerial; ++i) {
//				static class $cls01 {
//					   public static final long serialVersionUID = 0x75683BF5E5FC72L;     // in Class_1
				sb.append("static class $cls" + String.format("%02d", i+1) + "{\n");
				sb.append("   public static final long serialVersionUID = 0x");
				sb.append(String.format("%X", uid[i])).append("L;     // in Class_").append((i+1));
				sb.append("\n}\n");
			}
			sb.append("\n\n");
			sb.append("   // code to execute to decipher\n");
			sb.append("   long[] uids = new long[").append(nbSerial).append("];\n");
			for(int i = 0; i < nbSerial; ++i) {
				sb.append("   uids[").append(i).append("] = $classes.$cls").append(String.format("%02d", i+1)).append(".serialVersionUID;\n");
			}
			// and the translation
			sb.append("\n   // get translation using the method provided within that code\n");
			sb.append("   String str = decrypter(uids);\n");
		    codeText.setText(sb.toString());
		}

		public void insertUpdate(DocumentEvent e) {
			changedUpdate(e);
		}
		
		public void removeUpdate(DocumentEvent arg0) {
			changedUpdate(arg0);
		}
	}
}
