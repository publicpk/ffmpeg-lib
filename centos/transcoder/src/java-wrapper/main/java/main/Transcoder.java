package main;

import main.codec.cls.LoaderUtil;

class Transcoder {
	
	public static native void init(String logInfo);
	public static native void deinit();
	
	long _handle;
	
	static {
		new LoaderUtil();
	}
	
	public Transcoder(long uid) {
//		System.out.println("Wrapper's UID is " + uid);
	}
	
	public boolean open() {
		_handle = create();
		return (_handle != 0);
	}
	
	public void close() {
		destroy(_handle);
		_handle = 0;
	}
	
	public int process(int argc, String[] argv) {
		return core(argc, argv, _handle);
	}

	native long create();

	native void destroy(long handle);

	native int core(int argc, String[] argv, long handle);
	
	public native int getMetadata(String inputFile, AVMetadata metadata);

	//not used Api ----->
	public native void test(int val);
	
	public native int crop(String inputFile, String videoCodec, String audioCodec, 
			String containerFormat, int[] cropAreas, String[] outputFiles, String jobId);
	
	public native int transcode(String inputFile, String outputFile, 
			String videoCodec, String audioCodec, String containerFormat, String jobId);
	
	public native int scale(String inputFile, String outputFile, 
			String videoCodec, String audioCodec, String containerFormat, int videoWidth, 
			int videoHeight, String jobId);
}
