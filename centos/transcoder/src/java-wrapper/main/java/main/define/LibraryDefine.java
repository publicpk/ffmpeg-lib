package main.define;

public class LibraryDefine {
	public final static int RET_CODE_ERROR_EMPTY_PARAM = -101;
	public final static int RET_CODE_ERROR_INVALID_PARAM = -102;
	public final static int RET_CODE_ERROR_DATABASE = -103;
	public final static int RET_CODE_ERROR_SYSTEM = -104;
	public final static int RET_CODE_ERROR_SCALING = -105;
	public final static int RET_CODE_ERROR_CROPPING = -106;
	public final static int RET_CODE_COMMAND_SUCCESS = 0;
	public final static int RET_CODE_SUCCESS = 200;
	/**
	 * 	spec not define error --> -99 define
	 */
	public final static int RET_CODE_ERROR_LIBRARY_INTERNAL = -99;
	
	//cms server result msg ---->
	public final static String RET_MSG_ERROR_EMPTY_PARAM = "클라이언트 PARAMETER가 없습니다.";
	public final static String RET_MSG_ERROR_INVALID_PARAM = "클라이언트 PARAMETER 오류가 발생하였습니다.";
	public final static String RET_MSG_ERROR_DATABASE = "DB 오류가 발생하였습니다.";
	public final static String RET_MSG_ERROR_SYSTEM = "시스템 오류가 발생하였습니다.";
	public final static String RET_MSG_ERROR_SCALING = "해상도 변경 시 오류가 발생하였습니다.";
	public final static String RET_MSG_ERROR_CROPPING = "프레임 분할 시 오류가 발생하였습니다.";
	public final static String RET_MSG_ERROR_LIBRARY_INTERNAL = "동영상 라이브러리 내부에서 에러가 발생하였습니다.";
	public final static String RET_MSG_SUCCESS = "정상 처리되었습니다";
	//cms server result msg <----
	
	//Transcoder server internal result msg
	public final static String RET_MSG_ERROR_EMPTY_PARAM_INPUTFILE = "클라이언트 PARAMETER에 inputFile 정보가 없습니다.";
	public final static String RET_MSG_ERROR_EMPTY_PARAM_VIDEOCODEC = "클라이언트 PARAMETER에 videoCodec 정보가 없습니다.";
	public final static String RET_MSG_ERROR_EMPTY_PARAM_AUDIOCODEC = "클라이언트 PARAMETER에 audioCodec 정보가 없습니다.";
	public final static String RET_MSG_ERROR_EMPTY_PARAM_CONTAINERFORMAT = "클라이언트 PARAMETER에 containerFormat 정보가 없습니다.";
	public final static String RET_MSG_ERROR_EMPTY_PARAM_JOBID = "클라이언트 PARAMETER에 작업요청ID 정보가 없습니다.";
	public final static String RET_MSG_ERROR_EMPTY_PARAM_CROPAREAS = "클라이언트 PARAMETER에 Crop영역 정보가 없습니다.";
	public final static String RET_MSG_ERROR_EMPTY_PARAM_OUTPUTFILES = "클라이언트 PARAMETER에 outputFiles 정보가 없습니다.";
	public final static String RET_MSG_ERROR_EMPTY_PARAM_OUTPUTFILES_EACH = "클라이언트 PARAMETER에 %d번째 outputFile 정보가 없습니다.";
	
	public final static String RET_MSG_ERROR_INVALID_PARAM_VIDEOCODEC = "%s는 미지원 videoCodec으로 인하여 오류가 발생하였습니다.";
	public final static String RET_MSG_ERROR_INVALID_PARAM_AUDIOCODEC = "%s는 미지원 audioCodec으로 인하여 오류가 발생하였습니다.";
	public final static String RET_MSG_ERROR_INVALID_PARAM_CONTAINERFORMAT = "%s는 미지원 containerFormat으로 인하여 오류가 발생하였습니다.";
	public final static String RET_MSG_ERROR_INVALID_PARAM_NOTMATCH_CROPAREAS_OUTPUTFILES = "outputFiles 개수와 Crop영역 정보가 상이하여 오류가 발생하였습니다.";
	public final static String RET_MSG_ERROR_INVALID_PARAM_NOTEXIST_INPUTFILE = "inputFile %s가 존재하지 않아 오류가 발생하였습니다.";
	public final static String RET_MSG_ERROR_INVALID_PARAM_INPUTFILE = "inputFile %s는 오류가 있거나 동영상 정보가 없어서 오류가 발생하였습니다.";
	public final static String RET_MSG_ERROR_INVALID_PARAM_CROPSIZE = "Crop영상 %d넓이, %d높이 미지원 넓이, 높이로 인하여 오류가 발생하였습니다.";
	public final static String RET_MSG_ERROR_INVALID_PARAM_CROPAREAS = "Crop영역이 원본영상 영역을 이탈하여 오류가 발생하였습니다.";
}
