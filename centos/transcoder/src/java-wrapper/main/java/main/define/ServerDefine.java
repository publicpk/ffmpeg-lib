package main.define;

public class ServerDefine {
	public static final String SERVER_URL = "http://112.216.96.174:8080";
	
	
	public static final String TRANDCODING_PAGE = "/avdb/service/interface.do";
	public static final String SCALING_PAGE = "/avdb/service/interface.do";
	public static final String CROPING_PAGE = "/avdb/service/interface.do";
	
	
	public static final String CMD_PARAM = "cmd";
	public static final String API_PARAM = "api";
	public static final String EVT_NO_PARAM = "evt_no";
	public static final String SEC_YN_PARAM = "sec_yn";
	public static final String ERR_CODE_PARAM = "err_code";
	public static final String ERR_MSG_PARAM = "err_msg";
	
	
	public static final String CMD_VALUE = "201";
	
	public static final String API_TRANDCODING_VALUE = "101";
	public static final String API_SCALING_VALUE = "102";
	public static final String API_CROPING_VALUE = "103";
	
	public static final String SEC_Y_VALUE = "Y";
	public static final String SEC_N_VALUE = "N";
}
