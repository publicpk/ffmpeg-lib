package main;

class AVMetadata {
	public int hasVideoStream;
	
	public int hasAudioStream;
	
	public int width;
	
	public int height;
	
	public int videoCodecId;
	
	public int audioCodecId;
}
