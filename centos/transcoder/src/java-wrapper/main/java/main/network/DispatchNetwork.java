package main.network;

import main.util.HttpUtil;
import main.util.HttpUtil.HttpResult;

public class DispatchNetwork {
	public static HttpResult request(ImpNetwork a_oImpNetwork){
		HttpResult oHttpResult = new HttpResult();
		
		if(a_oImpNetwork != null) {
			HttpUtil oHttpUtil = new HttpUtil();
			oHttpResult = oHttpUtil.request(a_oImpNetwork.getPageUrl(), a_oImpNetwork.getMethod(), a_oImpNetwork.getRequestParam());
		}
		
		System.out.println("a_oImpNetwork.getPageUrl() = " + a_oImpNetwork.getPageUrl());
		System.out.println("oHttpResult.code = " + oHttpResult.m_nResultCode);
		System.out.println("oHttpResult.data = " + oHttpResult.m_sResultData);
		
		return oHttpResult;
	}
}
