package main.network;

import java.util.HashMap;

public interface ImpNetwork {
	public String getPageUrl();
	public String getMethod();
	public HashMap<String, String> getRequestParam();
}
