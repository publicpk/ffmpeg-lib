#include <jni.h>
#include <stdio.h>
#include <stdlib.h>

#include "main_Transcoder.h"
#include "transcode.h"

JNIEXPORT void JNICALL Java_main_Transcoder_init (JNIEnv * env, jobject obj, jstring logInfo) {
	const char* cLogInfo = (*env)->GetStringUTFChars(env, logInfo, 0);

	avtranscode_init(cLogInfo);

	if ( cLogInfo )
		(*env)->ReleaseStringUTFChars(env, logInfo, cLogInfo);
}

JNIEXPORT void JNICALL Java_main_Transcoder_deinit (JNIEnv * env, jobject obj) {
	avtranscode_deinit();
}

JNIEXPORT jlong JNICALL Java_main_Transcoder_create(JNIEnv *env, jobject obj) {
	return (jlong)avtranscode_create_job();
}

JNIEXPORT void JNICALL Java_main_Transcoder_destroy(JNIEnv *env, jobject obj, jlong instance) {
	avtranscode_destroy_job((TranscodeJob*)instance);
}

JNIEXPORT jint JNICALL Java_main_Transcoder_getMetadata
  (JNIEnv *env, jobject obj, jstring inputFile, jobject metadataObj)
{
	int ret;
	AVMetadata metadata;
	jfieldID fieldId;
	jclass clazz;
	const char* cInputFile = (*env)->GetStringUTFChars(env, inputFile, 0);

	ret = get_metadata(cInputFile, &metadata);

	// set data to java class
	clazz = (*env)->GetObjectClass(env, metadataObj);

	fieldId = (*env)->GetFieldID(env, clazz, "hasVideoStream", "I");
	(*env)->SetIntField(env, metadataObj, fieldId, metadata.hasVideoStream);
	if ( metadata.hasVideoStream != -1 ) {
		fieldId = (*env)->GetFieldID(env, clazz, "width", "I");
		(*env)->SetIntField(env, metadataObj, fieldId, metadata.width);
		fieldId = (*env)->GetFieldID(env, clazz, "height", "I");
		(*env)->SetIntField(env, metadataObj, fieldId, metadata.height);
		fieldId = (*env)->GetFieldID(env, clazz, "videoCodecId", "I");
		(*env)->SetIntField(env, metadataObj, fieldId, metadata.videoCodecId);
	}
	fieldId = (*env)->GetFieldID(env, clazz, "hasAudioStream", "I");
	(*env)->SetIntField(env, metadataObj, fieldId, metadata.hasAudioStream);
	if ( metadata.hasAudioStream != -1 ) {
		fieldId = (*env)->GetFieldID(env, clazz, "audioCodecId", "I");
		(*env)->SetIntField(env, metadataObj, fieldId, metadata.audioCodecId);
	}

	if ( cInputFile )
		(*env)->ReleaseStringUTFChars(env, inputFile, cInputFile);

	return ret;
}

JNIEXPORT jint JNICALL Java_main_Transcoder_core
  (JNIEnv *env, jobject obj, jint argc, jobjectArray argv, jlong instance)
{
	int res, i;
	const char** cargv = malloc(argc * sizeof(char*));
	const char** pcargv = cargv;
    for ( i=0; i<argc; i++) {
        jstring string = (jstring) (*env)->GetObjectArrayElement(env, argv, i);
        *pcargv++ = (*env)->GetStringUTFChars(env, string, 0);
        // Don't forget to call `ReleaseStringUTFChars` when you're done.
    }

    res = core_main(argc, (char**)cargv, (TranscodeJob*)instance);

	// release
	pcargv = cargv;
    for ( i=0; i<argc; i++) {
        jstring string = (jstring) (*env)->GetObjectArrayElement(env, argv, i);
    	(*env)->ReleaseStringUTFChars(env, string, *pcargv++);
    }

	return res;
}

//not use Api ----->
JNIEXPORT void JNICALL Java_main_Transcoder_test(JNIEnv *env, jobject obj, jint value) {
	printf("Hello... %d\n", value);
}

JNIEXPORT jint JNICALL Java_main_Transcoder_crop
  (JNIEnv *env, jobject obj, jstring inputFile, jstring videoCodec, jstring audioCodec,
		  jstring containerFormat, jintArray cropAreas, jobjectArray outputFiles, jstring jobId)
{
	return 0;
}

JNIEXPORT jint JNICALL Java_main_Transcoder_transcode
  (JNIEnv *env, jobject obj, jstring inputFile, jstring outputFile, jstring videoCodec, jstring audioCodec,
		  jstring containerFormat, jstring jobId)
{
	return 0;
}

JNIEXPORT jint JNICALL Java_main_Transcoder_scale
  (JNIEnv *env, jobject obj, jstring inputFile, jstring outputFile, jstring videoCodec, jstring audioCodec,
		  jstring containerFormat, jint videoWidth, jint videoHeight, jstring jobId)
{
	return 0;
}
