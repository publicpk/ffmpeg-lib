
# local.properties

```
    # proguard.jar location. It must be absolute path!!!
    # default value is /usr/share/java/proguard.jar on ubuntu
    proguard.jar=/path/to/proguard/lib/proguard.jar
    # library path for ffmpeg
    solibrary.path=/path/to/ffmpeg-build/lib
```

# install ProGuard(proguard.sourceforge.net)

```
    $ curl -L -O http://sourceforge.net/projects/proguard/files/proguard/5.2/proguard5.2.1.tar.gz
    $ tar -xzf proguard5.2.1.tar.gz
    $ sudo mv ./proguard5.2.1 /usr/share/java/
    $ sudo ln -sf /usr/share/java/proguard5.2.1 /usr/share/java/proguard
    $ sudo ln -sf /usr/share/java/proguard/lib/proguard.jar /usr/share/java/proguard.jar
    $ ls -al /usr/share/java/proguard.jar
```
