//============================================================================
// Name        : cxx-test.c
// Author      : tigle
// Version     :
// Copyright   : Your copyright notice
//============================================================================
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libavfilter/avfilter.h"
#include "libavfilter/buffersink.h"
#include "libavutil/avassert.h"
#include "libavutil/avstring.h"
#include "libavutil/bprint.h"
#include "libavutil/channel_layout.h"
#include "libavutil/opt.h"
#include "libavutil/pixdesc.h"
#include "libavutil/pixfmt.h"
#include "libavutil/imgutils.h"
#include "libavutil/samplefmt.h"
#include "libavutil/threadmessage.h"
#include "libavutil/eval.h"

//#include "cmdutils.h"

#include "transcode.h"

// 48: debug, 40: verbose, 32: info, 24: warning, 16: error, 8: fatal, 0: panic, -8: quiet
#define cLogInfo "file=%p-%t.log:level=24"

int test01();
int test02();

int main(int argc, char** argv) {
	int ret;

	printf("Start...\n");
	//setenv(REPORT_ENV_NAME, cLogInfo, 1);

	avtranscode_init(cLogInfo);

	//ret = test01();

	ret = test02();

	avtranscode_deinit();
	printf("Terminated...\n");
	return ret;
}

void* test02_threadjob(void *arg) {
	TranscodeJob* job = (TranscodeJob*)arg;
	int ret;

	if ( NULL == job ) {
		printf("Job is null\n");
		return NULL;
	}
	printf("test02: processing...%d\n", job->id);
	ret = core_main(job->argc, job->argv, job);

	printf("test02: RET(%d): %d\n", job->id, ret);

	avtranscode_destroy_job(job);
	return NULL;
}

void createarg_i1_o1(int* pargc, char*** pargv);
void createarg_i1_o2(int* pargc, char*** pargv);
void createarg_i1_o3o4(int* pargc, char*** pargv);

int test02() {
#define TEST02_THREAD_COUNT 3

	pthread_t test02_tid[TEST02_THREAD_COUNT];
	int ret;
	int count = TEST02_THREAD_COUNT;
	TranscodeJob *job;

    while(count-- > 0) {
    	job = avtranscode_create_job();
    	job->id = count;
    	switch ( count ) {
    	case 0: createarg_i1_o1(&job->argc, &job->argv); break;
    	case 1: createarg_i1_o2(&job->argc, &job->argv); break;
    	case 2: createarg_i1_o3o4(&job->argc, &job->argv); break;
    	default: break;
    	}
        ret = pthread_create(&(test02_tid[count]), NULL, &test02_threadjob, job);
        if (ret != 0) {
            printf("test02: can't create thread :[%s]\n", strerror(ret));
            avtranscode_destroy_job(job);
        }
        else
            printf("test02: Thread created successfully: %d\n", job->id);
    }

    count = TEST02_THREAD_COUNT;
    while(count-- > 0) {
    	pthread_join(test02_tid[count], NULL);
    }

	return ret;
}

int test01() {
	int argc;
	char** argv;
	int ret;

	TranscodeJob *job;
	printf ("test01: Processing 1...\n");

	job = avtranscode_create_job();
	createarg_i1_o1(&argc, &argv);
	ret = core_main(argc, argv, job);
	printf ("test01: RET1: %d\n", ret);
	avtranscode_destroy_job(job);

	printf ("test01: Processing 2...\n");
	job = avtranscode_create_job();
	createarg_i1_o2(&argc, &argv);
	ret = core_main(argc, argv, job);
	printf ("test01: RET2: %d\n", ret);

	avtranscode_destroy_job(job);

	return ret;
}

/*
-v error
-y
-i ./input1.mp4
-filter_complex "[0:v] crop=1000:720:0:0 [croped_0]"
-map \"[croped_0]\" -map 0:a -c:a libfdk_aac -c:v libx264 ./output1.mp4
*/
#define argv_i_o(_i, _o) \
{\
		"prog1",\
		"-y",\
		"-i", _i,\
		"-filter_complex", "[0:v] crop=200:200:0:0 [croped_0]",\
		"-map", "[croped_0]", "-map", "0:a", "-c:a", "libfdk_aac", "-c:v", "libx264", _o\
}

void createarg_i1_o1(int* pargc, char*** pargv) {
	static char* argv[] = argv_i_o("input1.mp4", "output1.mp4");
	static int argc = sizeof(argv) / sizeof(char*);
	*pargc = argc;
	*pargv = argv;
}

void createarg_i1_o2(int* pargc, char*** pargv) {
	static char* argv[] = argv_i_o("input1.mp4", "output2.mp4");
	static int argc = sizeof(argv) / sizeof(char*);
	*pargc = argc;
	*pargv = argv;
}

/*
-v error
-y
-i ./input1.mp4
-filter_complex "[0:v] crop=1000:720:0:0 [croped_0]; [0:v] rotate=45*PI/180:c=black [rotated_0]; [rotated_0] crop=280:720:1000:0 [croped_1]"
-map \"[croped_0]\" -map 0:a -c:a libfdk_aac -c:v libx264 ./output1.mp4
-map \"[croped_1]\" -c:v libx264 ./output2.mp4
*/
#define argv_i_oo(_i, _o1, _o2) \
{\
		"prog2",\
		"-y",\
		"-i", _i,\
		"-filter_complex", "[0:v] crop=1000:720:0:0 [croped_0]; [0:v] rotate=45*PI/180:c=black [rotated_0]; [rotated_0] crop=280:720:1000:0 [croped_1]",\
		"-map", "[croped_0]", "-map", "0:a", "-c:a", "libfdk_aac", "-c:v", "libx264", _o1,\
		"-map", "[croped_1]", "-c:v", "libx264", _o2\
}
void createarg_i1_o3o4(int* pargc, char*** pargv) {
	static char* argv[] = argv_i_oo("input1.mp4", "output3.mp4", "output4.mp4");
	static int argc = sizeof(argv) / sizeof(char*);
	*pargc = argc;
	*pargv = argv;
}

