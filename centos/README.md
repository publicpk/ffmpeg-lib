#
# Build FFmpeg library on CentOS
#

# Install Build Tools

```
	$ ./init_build_tools.sh	
```

# Edit config.sh

```
    $ ln -sf config/config-x264_only.sh config.sh
```

# Configure

```
    $ ./ffmpeg_configure.sh
```

# Build

```
    $ ./ffmepg_build.sh
```

# Misc

```
    # Build 3rd party libraries only
	$ ./build_3rd_libs.sh
```

# REF: http://trac.ffmpeg.org/wiki/CompilationGuide/Centos



