#!/bin/bash

source ./get_3rd_libs.sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

BUILD_DIR=$DIR/../build-ubuntu
INSTALL_PREFIX=$HOME/.local
TARGET_VERSION="n3.3.3"
#DEPS_PACKAGES=autoconf automake build-essential libass-dev libfreetype6-dev libsdl2-dev libtheora-dev libtool libva-dev libvdpau-dev libvorbis-dev libxcb1-dev libxcb-shm0-dev libxcb-xfixes0-dev pkg-config texinfo wget zlib1g-dev
DEPS_PACKAGES="autoconf automake build-essential libass-dev libfreetype6-dev libtheora-dev libtool libvorbis-dev pkg-config texinfo wget zlib1g-dev"


mkdir -p $BUILD_DIR
pushd $BUILD_DIR

#
# install dependency packages
# ***************************
# sudo apt-get -y install $DEPS_PACKAGES

#
# build dependencies
# ******************

# yasm
lib_name=yasm
lib_url=http://www.tortall.net/projects/yasm/releases/yasm-1.3.0.tar.gz
lib_url_type=tar.gz
lib_version=1.3.0
cur_version=$(find_bin $lib_name)
[[ $cur_version == "$lib_name $lib_version"* ]]; need_to_build=$?

echo ""
if [ $need_to_build -ne 0 ]; then
    echo "Build $lib_name"
    echo "*******************************"
    #get_3rd_lib $lib_url $lib_url_type $lib_version $lib_name
    lib_dir=$(get_3rd_lib $lib_url $lib_url_type $lib_version $lib_name)

    if [ -e "$lib_dir" ]; then
        pushd $lib_dir
        ./configure --prefix="$INSTALL_PREFIX" --bindir="$INSTALL_PREFIX/bin"
        make && make install
        popd
    else
        echo "!!! ERROR: error while getting $lib_name from $lib_url"
        echo ""
    fi
else
    echo "Found $lib_name $lib_version"
    echo "*******************************"
fi


# nasm
lib_name=nasm
lib_url=http://www.nasm.us/pub/nasm/releasebuilds/2.13.01/nasm-2.13.01.tar.bz2
lib_url_type=tar.bz2
lib_version=2.13.01
cur_version=$(find_bin $lib_name)
[[ $cur_version == "NASM version $lib_version"* ]]; need_to_build=$?

echo ""
if [ $need_to_build -ne 0 ]; then
    echo "Build $lib_name"
    echo "*******************************"
    lib_dir=$(get_3rd_lib $lib_url $lib_url_type $lib_version $lib_name)

    if [ -e "$lib_dir" ]; then
        pushd $lib_dir
        ./autogen.sh
        ./configure --prefix="$INSTALL_PREFIX" --bindir="$INSTALL_PREFIX/bin"
        make && make install
        popd
    else
        echo "!!! ERROR: error while getting $lib_name from $lib_url"
        echo ""
    fi
else
    echo "Found $lib_name $lib_version"
    echo "*******************************"
fi

# libx264
lib_name=x264
lib_url=http://download.videolan.org/pub/x264/snapshots/last_x264.tar.bz2
lib_url_type=tar.bz2
lib_version=snapshots
cur_version=$(find_package "x264")
[[ $cur_version == "0.152."* ]]; need_to_build=$?

echo ""
if [ $need_to_build -ne 0 ]; then
    echo "Build $lib_name"
    echo "*******************************"
    lib_dir=$(get_3rd_lib $lib_url $lib_url_type $lib_version $lib_name)

    if [ -e "$lib_dir" ]; then
        pushd $lib_dir
        ./configure --prefix="$INSTALL_PREFIX" --bindir="$INSTALL_PREFIX/bin" --enable-static --disable-opencl
        make && make install
        popd
    else
        echo "!!! ERROR: error while getting $lib_name from $lib_url"
        echo ""
    fi
else
    echo "Found $lib_name $cur_version"
    echo "*******************************"
fi


# libx265
lib_name=x265
lib_url=https://bitbucket.org/multicoreware/x265
lib_url_type=hg
lib_version=0 # master
cur_version=$(find_package "x265")
[[ $cur_version == "2.5"* ]]; need_to_build=$?

echo ""
if [ $need_to_build -ne 0 ]; then
    echo "Build $lib_name"
    echo "*******************************"
    lib_dir=$(get_3rd_lib $lib_url $lib_url_type $lib_version $lib_name)

    if [ -e "$lib_dir" ]; then
        pushd $lib_dir/build/linux
        cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX="$INSTALL_PREFIX" -DENABLE_SHARED:bool=off ../../source
        make && make install
        popd
    else
        echo "!!! ERROR: error while getting $lib_name from $lib_url"
        echo ""
    fi
else
    echo "Found $lib_name $cur_version"
    echo "*******************************"
fi


# libfdk-aac
lib_name=fdk-aac
lib_url=https://github.com/mstorsjo/fdk-aac/tarball/master
lib_url_type=tar.gz
lib_version=master
cur_version=$(find_package $lib_name)
[[ $cur_version == "0.1.5"* ]]; need_to_build=$?

echo ""
if [ $need_to_build -ne 0 ]; then
    echo "Build $lib_name"
    echo "*******************************"
    lib_dir=$(get_3rd_lib $lib_url $lib_url_type $lib_version $lib_name)

    if [ -e "$lib_dir" ]; then
        pushd $lib_dir
        autoreconf -fiv
        ./configure --prefix="$INSTALL_PREFIX" --disable-shared
        make && make install
        popd
    else
        echo "!!! ERROR: error while getting $lib_name from $lib_url"
        echo ""
    fi
else
    echo "Found $lib_name $cur_version"
    echo "*******************************"
fi


# libmp3lame
lib_name=mp3lame
lib_url=http://downloads.sourceforge.net/project/lame/lame/3.99/lame-3.99.5.tar.gz
lib_url_type=tar.gz
lib_version=3.99.5
cur_version=$(find_package $lib_name)
[[ $cur_version == "$lib_version" ]]; need_to_build=$?

echo ""
if [ $need_to_build -ne 0 ]; then
    echo "Build $lib_name"
    echo "*******************************"
    lib_dir=$(get_3rd_lib $lib_url $lib_url_type $lib_version $lib_name)

    if [ -e "$lib_dir" ]; then
        pushd $lib_dir
        ./configure --prefix="$INSTALL_PREFIX" --enable-nasm --disable-shared
        make && make install
        popd
        # generate .pc file
        generage_pkgconfig $INSTALL_PREFIX/lib/pkgconfig/$lib_name.pc $INSTALL_PREFIX $lib_name $lib_version "lame"
    else
        echo "!!! ERROR: error while getting $lib_name from $lib_url"
        echo ""
    fi
else
    echo "Found $lib_name $cur_version"
    echo "*******************************"
fi


# libopus
lib_name=opus
lib_url=https://archive.mozilla.org/pub/opus/opus-1.1.5.tar.gz
lib_url_type=tar.gz
lib_version=1.1.5
cur_version=$(find_package $lib_name)
[[ $cur_version == "$lib_version" ]]; need_to_build=$?

echo ""
if [ $need_to_build -ne 0 ]; then
    echo "Build $lib_name"
    echo "*******************************"
    lib_dir=$(get_3rd_lib $lib_url $lib_url_type $lib_version $lib_name)

    if [ -e "$lib_dir" ]; then
        pushd $lib_dir
        ./configure --prefix="$INSTALL_PREFIX" --disable-shared
        make && make install
        popd
    else
        echo "!!! ERROR: error while getting $lib_name from $lib_url"
        echo ""
    fi
else
    echo "Found $lib_name $cur_version"
    echo "*******************************"
fi


# libvpx
lib_name=vpx
lib_url=https://chromium.googlesource.com/webm/libvpx.git
lib_url_type=git
lib_version=0 # master
cur_version=$(find_package $lib_name)
[[ $cur_version == "1.6.1" ]]; need_to_build=$?

echo ""
if [ $need_to_build -ne 0 ]; then
    echo "Build $lib_name"
    echo "*******************************"
    lib_dir=$(get_3rd_lib $lib_url $lib_url_type $lib_version $lib_name)

    if [ -e "$lib_dir" ]; then
        pushd $lib_dir
        ./configure --prefix="$INSTALL_PREFIX" --disable-examples --disable-unit-tests --enable-vp9-highbitdepth
        make && make install
        popd
    else
        echo "!!! ERROR: error while getting $lib_name from $lib_url"
        echo ""
    fi
else
    echo "Found $lib_name $cur_version"
    echo "*******************************"
fi


# ffmpeg
lib_name=ffmpeg
lib_url_type=git
lib_version=n3.3.3
cur_version=$(find_bin $lib_name "-version")
[[ $cur_version == "ffmpeg version $lib_version"* ]]; need_to_build=$?
lib_dir=../ffmpeg.git
echo ""
if [ $need_to_build -ne 0 ]; then
    pushd $lib_dir
    #git pull
    git checkout $lib_version

    ./configure \
        --prefix="$INSTALL_PREFIX" \
        --pkg-config-flags="--static" \
        --extra-cflags="-I$INSTALL_PREFIX/include" \
        --extra-ldflags="-L$INSTALL_PREFIX/lib" \
        --bindir="$INSTALL_PREFIX/bin" \
        --enable-gpl \
        --enable-libass \
        --enable-libfdk-aac \
        --enable-libfreetype \
        --enable-libmp3lame \
        --enable-libopus \
        --enable-libtheora \
        --enable-libvorbis \
        --enable-libvpx \
        --enable-libx264 \
        --enable-libx265 \
        --enable-nonfree
    make && make install
    hash -r
    popd
else
    echo "Found $lib_name $cur_version"
    echo "*******************************"
fi
