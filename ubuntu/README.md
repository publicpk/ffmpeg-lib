# Build FFmpeg on Ubuntu #

## install build tool packages
```
sudo apt install build-essential autoconf automake libtool cmake mercurial pkg-config texinfo wget 
```
## install dependencies
```
ESSENTIAL_DEPS_PACKAGES="libass-dev libfreetype6-dev libtheora-dev libvorbis-dev zlib1g-dev"
EXTRA_DEPS_PACKAGES="libsdl2-dev libva-dev libvdpau-dev libxcb1-dev libxcb-shm0-dev libxcb-xfixes0-dev"

sudo apt install $ESSENTIAL_DEPS_PACKAGES
```

## build
```
cd ubuntu
bash ./build.sh

