#!/bin/bash

DEBUG=0
TAG="get_3rd_lib"
function decho() {
    if [ $DEBUG == 1 ]; then
        echo "${TAG}:$1"
    fi
}
#
# Download 3rd-party libraries
#
function get_3rd_lib() {
	local url="$1"
	local extension="$2"
	local version="$3"
	local name="$4"

	local savename=${url##*/}
	local result=0

	if [ "x${extension}" == "x" ]; then
		# extension is not specified
		# try to find the extension from the url
		# extension=${url##*/}
		extension=${savename##*.}
	fi
	[ "x${extension}" == "x" ] && decho "!!! no extention specified" && exit 1

	if [ "x${name}" == "x" ]; then
		# name is not specified
		# try to find the name from the url
		#name=${url##*/}
		name=${savename%.*}
		# remove .tar
		[ ${name##*.} == "tar" ] && name=${name%.*}
	fi
	# [ "x${name}" == "x" ] && decho "!!! no name specified" && exit 1

	# when the extension is git or hg, 
	# - append "-git" if has specific version
	# - append "-master" if not
	if [[ "${extension}" == "git" || "${extension}" == "hg" ]]; then
        name="${name}.${extension}"
	else
		[ "x${version}" != "x0" ] && name="${name}-${version}"
	fi

	decho ">>> url is '${url}'"
	decho ">>> version is '${version}'"
	decho ">>> extension is '${extension}'"
	decho ">>> name is '${name}'"

	#
	# Download sources
	#
	function _download() {
		result=0
		case $extension in
			git ) 
                if [ ! -e "$name" ]; then
                    git clone $url $name > /dev/null
                fi
                pushd $name > /dev/null
                git pull > /dev/null
                if [ "x${version}" != "x0" ]; then
                    git checkout $version > /dev/null
                fi
                popd > /dev/null
				;;
			hg )
                if [ ! -e "$name" ]; then
                    hg clone $url $name > /dev/null
                fi
                pushd $name > /dev/null
                hg pull > /dev/null
                if [ "x${version}" != "x0" ]; then
                    hg up $version > /dev/null
                fi
                popd > /dev/null
				;;
            tar.bz2 ) ;& # fall through
            bz2 ) [ -e $savename ] || curl -L $url -o $savename
				mkdir -p $name
				tar -xjf $savename -C $name --strip 1
				;;
            tar.gz ) ;& # fall through
			gz ) [ -e $savename ] || curl -L $url -o $savename
				mkdir -p $name
				tar -xzf $savename -C $name --strip 1
				;;
			xz ) [ -e $savename ] || curl -L $url -o $savename
				mkdir -p $name
				tar -xJf $savename -C $name --strip 1
				;;
			tar ) # TODO
				;&
			* ) 
				decho "!!! invalid extension: ${extension}"
				result=1
				;;
		esac
	}

	if [ -e $name ]; then
		decho ">>> '${name}' is already existed..."
		if [[ "$extension" == "git" || "$extension" == "hg" ]]; then
            decho ">>> Download..."
            _download
		    result=$?
        else
		    result=0
		fi
	else
        decho ">>> Download..."
        _download
		result=$?
	fi
    decho ">>> result: $result, name: $name"
	# if success, return directory name
	if [ "x${result}" == "x0" ]; then
		echo "$name"
	else
		echo ""
	fi
}

function find_package() {
    local name="$1"
    local result=$(pkg-config --modversion --silence-errors $name)
    # local result=$(pkg-config --exists $name)
    echo $result
}

function find_bin() {
    local name="$1"
    local ver_arg="--version"
    if [ "$2" != "" ]; then
        ver_arg="$2"
    fi
    #local result=$($name $ver_arg 2> /dev/null | awk 'NR==1; END{print}') # return the first line of output
    local result=$($name $ver_arg 2> /dev/null | sed -e 1b -e '$!d') # return the first line of output
    echo $result
}

function generage_pkgconfig() {
    local output="$1"
    local prefix="$2"
    local name="$3"
    local version="$4"
    local inc_dir="$5"
    
    cat > $output <<EOF
prefix=${prefix}
exec_prefix=\${prefix}
libdir=\${exec_prefix}/lib
includedir=\${prefix}/include

Name: ${name}
Description: 
URL: 
Version: ${version}
Requires:
Conflicts:
Libs: -L\${libdir} -l${name}
Libs.private: -lm
Cflags: -I\${includedir}/${inc_dir}
EOF
}

