#!/bin/bash

# /////////////////////////////////
# Screenshot
#
INPUT1=../../input/video_1280x720_mp4_h264_1000kbps_25fps_aac_stereo_128kbps_44100hz.mp4
SCREENSHOT_TIME1="-ss 00:00:01" # 1 second
SCREENSHOT_TIME2="-ss 00:00:05" # 5 second
SCREENSHOT_TIME3="-ss 00:00:05.680" # 5 second
#SCREENSHOT_SIZE="-s 1280x720"
OUTPUT1=./output/screenshot_1.png
OUTPUT2=./output/screenshot_2.png
OUTPUT3=./output/screenshot_3.png

mkdir -p ./output

export LD_LIBRARY_PATH=~/ffmpeg-build/lib
STARTTIME=$(date +%s%N)
ffmpeg -v error -y -i $INPUT1 -vframes 1 $SCREENSHOT_TIME1 $SCREENSHOT_SIZE $OUTPUT1
ffmpeg -v error -y -i $INPUT1 -vframes 1 $SCREENSHOT_TIME2 $SCREENSHOT_SIZE $OUTPUT2
ffmpeg -v error -y -i $INPUT1 -vframes 1 $SCREENSHOT_TIME3 $SCREENSHOT_SIZE $OUTPUT3
ELAPSEDTIME=$((($(date +%s%N) - $STARTTIME)/1000000))
echo "Elapsed time: $ELAPSEDTIME milliseconds..."
