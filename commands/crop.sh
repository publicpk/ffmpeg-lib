#!/bin/bash

# Crop: http://ffmpeg.org/ffmpeg-filters.html#crop

CMDS="crop_1 crop_2"

CMD="crop_2"

INPUT=../../input/video_1280x720_mp4_h264_1000kbps_25fps_aac_stereo_128kbps_44100hz.mp4
INPUT=../../input/video_1280x720_webm_vp9_309kbps_25fps_vorbis_stereo_128kbps_44100hz.webm
OUTPUT_DIR="./output"
OUTPUT1="${OUTPUT_DIR}/${CMD}_out1.mp4"
OUTPUT2="${OUTPUT_DIR}/${CMD}_out2.mp4"
OUTPUT3="${OUTPUT_DIR}/${CMD}_out3.mp4"

#rm -f $OUTPUT1 $OUTPUT2 $OUTPUT3

export LD_LIBRARY_PATH=~/ffmpeg-build/lib

STARTTIME=$(date +%s%N)
if [ "$CMD" == "crop_1" ]; 
then
	x=442
	y=142
	w=261
	h=469

	ffmpeg -v error -y -i $INPUT -filter:v "\
	crop=${w}:${h}:${x}:${y} \
	" -c:a copy $OUTPUT1

elif [ "$CMD" == "crop_2" ]; 
then
	x1=0
	y1=0
	w1=300
	h1=300

	x2=300
	y2=0
	w2=300
	h2=300

	# x=500, y=300, width=100, height=120
	ffmpeg -v error -y -i $INPUT -filter_complex "\
	[0:v] crop=${w1}:${h1}:${x1}:${y1} [croped_1]; \
	[0:v] crop=${w2}:${h2}:${x2}:${y2} [croped_2] \
	" -map "[croped_1]" -map 0:a -c:a libfdk_aac -c:v libx264 $OUTPUT1 -map "[croped_2]" -c:v libx264 $OUTPUT2
else
	echo "No command..."
	OUTPUT1=""
fi
ELAPSEDTIME=$((($(date +%s%N) - $STARTTIME)/1000000))
echo "Elapsed time: $ELAPSEDTIME milliseconds..."

if [ "$OUTPUT1" != "" ]; then
	ffprobe -v error -i $OUTPUT1 -print_format json -show_format -show_streams
fi

# ffmpeg -i input01.mp4 -i input02.mp4 -filter_complex "\
# nullsrc=size=200x200 [background]; \
# [0:v] setpts=PTS-STARTPTS, scale=100x100 [left]; \
# [1:v] setpts=PTS-STARTPTS, scale=100x100 [right]; \
# [background][left] overlay=shortest=1 [background+left]; \
# [background+left][right] overlay=shortest=1:x=100 [left+right] \
# " -map "[left+right]" output.mp4

# ffmpeg -i input01.mp4 -i input02.mp4 -filter_complex "\
# [0:v] scale=iw/2:ih/2[left]; \
# [1:v] scale=iw/2:ih/2[right]; \
# [left][right]overlay=main_w/2:0 [out] \
# " -map "[out]" output.mp4
