#!/bin/bash

CMDS="mix_3 rotate_1"

CMD="rotate_1"

INPUT1=../../input/video_1280x720_mp4_h264_1000kbps_25fps_aac_stereo_128kbps_44100hz.mp4
INPUT2=$INPUT1
INPUT3=$INPUT1

OUTPUT="./output/output_$CMD.mp4"

rm -f $OUTPUT

if [ "$CMD" == "mix_3" ]; 
then
	# Overlay: https://www.ffmpeg.org/ffmpeg-all.html#overlay-1

	# 3 input, horizontal
	ffmpeg -v error -i $INPUT1 -i $INPUT2 -i $INPUT3 -filter_complex "\
	color=c=black@0.2:size=340x200 [bg]; \
	[0:v] setpts=PTS-STARTPTS, scale=100x100 [v0]; \
	[1:v] setpts=PTS-STARTPTS, scale=100x100 [v1]; \
	[2:v] setpts=PTS-STARTPTS, scale=100x100 [v2]; \
	[bg][v0] overlay=shortest=1:x=0 [bg+v0]; \
	[bg+v0][v1] overlay=shortest=1:x=100 [v0+v1]; \
	[v0+v1][v2] overlay=shortest=1:x=200 [v1+v2] \
	" -map "[v1+v2]" $OUTPUT

elif [ "$CMD" == "rotate_1" ]; 
then
	# Rotate: https://www.ffmpeg.org/ffmpeg-all.html#rotate

	# 1 input, 45 degree rotation
	newsize=170 #$(echo "sqrt (100)" | bc -l)
	echo "output size: ${newsize}x${newsize}"
	ffmpeg -v error -i $INPUT1 -filter_complex "\
	color=c=red@0.2:size=${newsize}x${newsize} [bg]; \
	[0:v] rotate=-45*PI/180:c=black:ow=oh, scale=100x100 [v0]; \
	[bg][v0] overlay=shortest=1:x=20:y=20 [bg+v0] \
	" -map "[bg+v0]" $OUTPUT

#[1:v]rotate=-30*PI/180:c=none:ow=rotw(iw):oh=roth(ih) [rotate];
#[0:v][rotate] overlay=40:10

else
	echo "No command..."
	OUTPUT=""
fi

if [ "$OUTPUT" != "" ]; then
	ffprobe -v error -i $OUTPUT -print_format json -show_format
fi


# ffmpeg -i input01.mp4 -i input02.mp4 -filter_complex "\
# nullsrc=size=200x200 [background]; \
# [0:v] setpts=PTS-STARTPTS, scale=100x100 [left]; \
# [1:v] setpts=PTS-STARTPTS, scale=100x100 [right]; \
# [background][left] overlay=shortest=1 [background+left]; \
# [background+left][right] overlay=shortest=1:x=100 [left+right] \
# " -map "[left+right]" output.mp4

# ffmpeg -i input01.mp4 -i input02.mp4 -filter_complex "\
# [0:v] scale=iw/2:ih/2[left]; \
# [1:v] scale=iw/2:ih/2[right]; \
# [left][right]overlay=main_w/2:0 [out] \
# " -map "[out]" output.mp4
