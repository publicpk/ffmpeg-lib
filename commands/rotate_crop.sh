#!/bin/bash

CMDS="rcrop_1 rcrop_2"

CMD="rcrop_2"

INPUT=../../input/video_1280x720_mp4_h264_1000kbps_25fps_aac_stereo_128kbps_44100hz.mp4
OUTPUT_DIR="./output"
OUTPUT1="${OUTPUT_DIR}/${CMD}_out1.mp4"
OUTPUT2="${OUTPUT_DIR}/${CMD}_out2.mp4"
OUTPUT3="${OUTPUT_DIR}/${CMD}_out3.mp4"

rm -f $OUTPUT1 $OUTPUT2 $OUTPUT3

export LD_LIBRARY_PATH=~/ffmpeg-build/lib

if [ "$CMD" == "rcrop_1" ]; 
then
	# Crop: http://ffmpeg.org/ffmpeg-filters.html#crop

	# rotate input and crop it
	# x=500, y=300, width=100, height=120
	x=339
	y=85
	w=535
	h=481
	rotate="45*PI/180" # 45 degree, clockwise

	ffmpeg -v error -y -i $INPUT -filter_complex "\
	[0:v] rotate=${rotate}:c=black [v0]; \
	[v0] crop=${w}:${h}:${x}:${y} [r0] \
	" -map "[r0]" $OUTPUT1

#[1:v]rotate=-30*PI/180:c=none:ow=rotw(iw):oh=roth(ih) [rotate];
#[0:v][rotate] overlay=40:10

	# ffmpeg -v error -i $INPUT -filter:v "\
	# crop=100:120:500:300 \
	# " -c:a copy $OUTPUT1


elif [ "$CMD" == "rcrop_2" ]; 
then
	x1=0
	y1=0
	w1=300
	h1=300
	r1=45

	x2=300
	y2=0
	w2=300
	h2=300
	r2=-45

	ffmpeg -v error -y -i $INPUT -filter_complex "\
	[0:v] rotate=${r1}*PI/180:c=black [rotated_1]; \
	[rotated_1] crop=${w1}:${h1}:${x1}:${y1} [croped_1]; \
	[0:v] rotate=${r2}*PI/180:c=black [rotated_2]; \
	[rotated_2] crop=${w2}:${h2}:${x2}:${y2} [croped_2] \
	" -map "[croped_1]" -c:a copy $OUTPUT1 -map "[croped_2]" $OUTPUT2
else
	echo "No command..."
	OUTPUT1=""
fi

if [ "$OUTPUT1" != "" ]; then
	ffprobe -v error -i $OUTPUT1 -print_format json -show_format
fi


# ffmpeg -i input01.mp4 -i input02.mp4 -filter_complex "\
# nullsrc=size=200x200 [background]; \
# [0:v] setpts=PTS-STARTPTS, scale=100x100 [left]; \
# [1:v] setpts=PTS-STARTPTS, scale=100x100 [right]; \
# [background][left] overlay=shortest=1 [background+left]; \
# [background+left][right] overlay=shortest=1:x=100 [left+right] \
# " -map "[left+right]" output.mp4

# ffmpeg -i input01.mp4 -i input02.mp4 -filter_complex "\
# [0:v] scale=iw/2:ih/2[left]; \
# [1:v] scale=iw/2:ih/2[right]; \
# [left][right]overlay=main_w/2:0 [out] \
# " -map "[out]" output.mp4
