#!/bin/bash

export LD_LIBRARY_PATH=~/ffmpeg-build/lib

# See https://trac.ffmpeg.org/wiki/Encode/H.264

FFMPEG="ffmpeg -v error -y"

# INPUT1=../../input/video_1280x720_mp4_h264_1000kbps_25fps_aac_stereo_128kbps_44100hz.mp4
INPUT1=~/Videos/tmp/sado.mp4

OUTPUT1=./output/1.mp4

x=0
y=0
w=1080
h=1920

STARTTIME=$(date +%s%N)

$FFMPEG -i $INPUT1                     \
        -filter:v "                    \
        	crop=${w}:${h}:${x}:${y}           \
        	"                                  \
        -c:a copy $OUTPUT1

ELAPSEDTIME=$((($(date +%s%N) - $STARTTIME)/1000000))
echo "Elapsed time: $ELAPSEDTIME milliseconds..."
