#!/bin/bash

export LD_LIBRARY_PATH=~/ffmpeg-build/lib

# INPUT1=../../input/video_1280x720_mp4_h264_1000kbps_25fps_aac_stereo_128kbps_44100hz.mp4
INPUT1=~/Videos/tmp/sado.mp4

OUTPUT1=./output/1.mp4
OUTPUT2=./output/2.mp4
OUTPUT3=./output/3.mp4

x=0
y=0
w=1080
h=1920

STARTTIME=$(date +%s%N)

ffmpeg -v error -y -i $INPUT1 -filter:v "\
	crop=${w}:${h}:${x}:${y} \
	" -c:a copy $OUTPUT1

# print available codecs
#ffmpeg -codecs > codecs.txt

# Use libx264 for encoding the first video stream and
# use fdk-aac for encodning the first audio stream
#ffmpeg -v error -y -i $INPUT1 -c:v:0 libx264 -c:a:0 libfdk_aac $OUTPUT1

# simple filter
# ffmpeg -v error -y -i $INPUT1 -filter:v "\
# 	split [main][tmp]; \
# 	[tmp] crop=iw:ih/2:0:0, vflip [flip]; \
# 	[main][flip] overlay=0:H/2 \
# 	" $OUTPUT1
#

# ffmpeg -v error -y -i $INPUT1 -filter_complex:v "\
# 	split [main][tmp]; \
# 	[tmp] crop=iw:ih/2:0:0, vflip [flip]; \
# 	[0:v] crop=iw:ih/2:0:0, vflip [flip2]; \
# 	[main][flip] overlay=0:H/2 [main+flip]\
# 	" -map "[main+flip]" $OUTPUT1 -map "[flip2]" $OUTPUT2


ELAPSEDTIME=$((($(date +%s%N) - $STARTTIME)/1000000))
echo "Elapsed time: $ELAPSEDTIME milliseconds..."
