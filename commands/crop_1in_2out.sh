#!/bin/bash

export LD_LIBRARY_PATH=~/ffmpeg-build/lib
FFMPEG="ffmpeg -v error -y"
# INPUT1=../../input/video_1280x720_mp4_h264_1000kbps_25fps_aac_stereo_128kbps_44100hz.mp4
INPUT1=~/Videos/tmp/sado.mp4

OUTPUT1=./output/h264-1.mp4
OUTPUT2=./output/h264-2.mp4

PRESET="ultrafast"
# PRESET="superfast"
# PRESET="veryfast"
# PRESET="veryslow"
CRF=24

x=0
y=0
w=1080
h=1920

x1=0
y1=0
w1=1080
h1=1920

x2=0
y2=1920
w2=1080
h2=1920

STARTTIME=$(date +%s%N)

$FFMPEG -i $INPUT1 -filter_complex "\
	[0:v] crop=${w1}:${h1}:${x1}:${y1} [croped_1]; \
	[0:v] crop=${w2}:${h2}:${x2}:${y2} [croped_2] \
	" \
    -map "[croped_1]" -map 0:a -c:a copy -c:v libx264 -preset $PRESET -crf $CRF $OUTPUT1 \
    -map "[croped_2]" -map 0:a -c:a copy -c:v libx264 -preset $PRESET -crf $CRF $OUTPUT2

ELAPSEDTIME=$((($(date +%s%N) - $STARTTIME)/1000000))

echo "Elapsed time: $ELAPSEDTIME milliseconds..."
