#!/bin/bash
INPUT1=../../input/video_1280x720_mp4_h264_1000kbps_25fps_aac_stereo_128kbps_44100hz.mp4
OUTPUT=./output/codec_info.txt

ffprobe -v error -i $INPUT1 -print_format json -show_format -show_streams | tee $OUTPUT
